// ET_Reader.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include "eye_tracker_reader.h"

int _tmain(int argc, _TCHAR* argv[])
{
	EyeTrackerReader ET = EyeTrackerReader::Instance();

	ET.Aquire();

	int FrameCnt = 0;
	int nFrame   = 0;
	while (FrameCnt < 1000)
	{
		Sleep(5);
		Eye_tracker_data ET_Data = ET.GetEyeTrackerData();
		if (ET_Data.frame_number != nFrame)
		{
			FrameCnt++;

			nFrame = ET_Data.frame_number;
			std::cout<<"FrameNumber = "<<nFrame<<std::endl;
			std::cout<<"PoG_Left = ("<<ET_Data.left_eye_point_of_gaze_estimate.x_in_pixels<<","<<ET_Data.left_eye_point_of_gaze_estimate.y_in_pixels<<")"<<std::endl;
			std::cout<<"PoG_Right = ("<<ET_Data.right_eye_point_of_gaze_estimate.x_in_pixels<<","<<ET_Data.right_eye_point_of_gaze_estimate.y_in_pixels<<")"<<std::endl;
		}

	}

	ET.Release();

	return 0;
}

