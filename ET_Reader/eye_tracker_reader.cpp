#include "stdafx.h"
#include <cassert>
#include <cstring>
#include <iomanip>
#include "eye_tracker_reader.h"



EyeTrackerReader::EyeTrackerReader()
    : _p_eye_tracker_memory(nullptr), _memory_mapped_file(NULL), _memory_update_timer(NULL),
      _new_eye_tracker_data_event(NULL)
{
  memset(&_eye_tracker_data, 0, sizeof(_eye_tracker_data));

  DEVMODE devmode;
  memset(&devmode, 0, sizeof(devmode));
  devmode.dmSize = sizeof(devmode);
  EnumDisplaySettings(NULL, ENUM_CURRENT_SETTINGS, &devmode);
  _stimuli_monitor_x_position = devmode.dmPosition.x;
  _stimuli_monitor_y_position = devmode.dmPosition.y;
  _stimuli_monitor_width = devmode.dmPelsWidth;
  _stimuli_monitor_height = devmode.dmPelsHeight;
}


EyeTrackerReader& EyeTrackerReader::Instance()
{
  static EyeTrackerReader eye_tracker;
  return eye_tracker;
}


bool EyeTrackerReader::Aquire()
{
  OutputDebugStringA("Acquiring Eye Tracker");
  uint32_t mapping_attempts = 0;

  _memory_mapped_file = OpenFileMapping(FILE_MAP_READ, FALSE, L"MyFileMappingObject");

  while((_memory_mapped_file == NULL) && (mapping_attempts < 5))
  {
    mapping_attempts++;
    Sleep(1000);
    _memory_mapped_file = OpenFileMapping(FILE_MAP_READ, FALSE, L"MyFileMappingObject");
  }

  if(_memory_mapped_file == NULL) return false;

  OutputDebugStringA("Opened Eye Tracker mapped memory");

  _p_eye_tracker_memory =
      reinterpret_cast<Eye_tracker_data*>(MapViewOfFile(_memory_mapped_file, FILE_MAP_READ, 0, 0, 0));

  if(_p_eye_tracker_memory == nullptr)
  {
    CloseHandle(_memory_mapped_file);
    _memory_mapped_file = NULL;

    return false;
  }

  OutputDebugStringA("Mapped Eye Tracker mapped memory");

  memcpy(&_eye_tracker_data, _p_eye_tracker_memory, sizeof(_eye_tracker_data));

  while(_eye_tracker_data.frame_number == -1.0)
  {
    // Wait for shared memory to be correctly populated
    OutputDebugStringA("Waiting for valid Eye Tracker memory");
    Sleep(30);
    memcpy(&_eye_tracker_data, _p_eye_tracker_memory, sizeof(_eye_tracker_data));
  }

  OutputDebugStringA("Eye Tracker memory is valid");

  uint32_t monitor_index = static_cast<uint32_t>(_eye_tracker_data.visual_stimuli_monitor_index);
  DISPLAY_DEVICE display_device;
  memset(&display_device, 0, sizeof(display_device));
  display_device.cb = sizeof(display_device);
  EnumDisplayDevices(NULL, monitor_index, &display_device, 0);
  DEVMODE devmode;
  memset(&devmode, 0, sizeof(devmode));
  devmode.dmSize = sizeof(devmode);
  EnumDisplaySettings(display_device.DeviceName, ENUM_CURRENT_SETTINGS, &devmode);
  _stimuli_monitor_x_position = devmode.dmPosition.x;
  _stimuli_monitor_y_position = devmode.dmPosition.y;
  _stimuli_monitor_width = devmode.dmPelsWidth;
  _stimuli_monitor_height = devmode.dmPelsHeight;

  timeBeginPeriod(1);

  if(CreateTimerQueueTimer(&_memory_update_timer, NULL, WaitOrTimerCallback, this, 0, 1, WT_EXECUTEINTIMERTHREAD) ==
     FALSE)
  {
    timeEndPeriod(1);

    UnmapViewOfFile(_p_eye_tracker_memory);
    _p_eye_tracker_memory = nullptr;

    CloseHandle(_memory_mapped_file);
    _memory_mapped_file = NULL;

    return false;
  }

  _new_eye_tracker_data_event = CreateEvent(NULL, FALSE, FALSE, L"NewEyeTrackerDataEvent");

  OutputDebugStringA("Eye Tracker acquired");

  return true;
}

void EyeTrackerReader::Release()
{
  assert(_memory_mapped_file != NULL);
  assert(_p_eye_tracker_memory != nullptr);
  assert(_memory_update_timer != NULL);

  BOOL ret = DeleteTimerQueueTimer(NULL, _memory_update_timer, INVALID_HANDLE_VALUE);

  if((ret == FALSE) && GetLastError() != ERROR_IO_PENDING)
  {
    ret = DeleteTimerQueueTimer(NULL, _memory_update_timer, INVALID_HANDLE_VALUE);
  }

  _memory_update_timer = NULL;

  timeEndPeriod(1);

  UnmapViewOfFile(_p_eye_tracker_memory);
  _p_eye_tracker_memory = nullptr;

  CloseHandle(_memory_mapped_file);
  _memory_mapped_file = NULL;

  CloseHandle(_new_eye_tracker_data_event);
}

VOID CALLBACK EyeTrackerReader::WaitOrTimerCallback(PVOID lpParameter, BOOLEAN)
{
  EyeTrackerReader* p_eye_tracker = reinterpret_cast<EyeTrackerReader*>(lpParameter);
  if(p_eye_tracker->_p_eye_tracker_memory &&
     (p_eye_tracker->_p_eye_tracker_memory->frame_number != p_eye_tracker->_eye_tracker_data.frame_number))
  {
    memcpy(&(p_eye_tracker->_eye_tracker_data), p_eye_tracker->_p_eye_tracker_memory,
           sizeof(p_eye_tracker->_eye_tracker_data));
    SetEvent(p_eye_tracker->_new_eye_tracker_data_event);
  }
}

int32_t EyeTrackerReader::GetEyeTrackerStimuliMonitorXPosition()
{
  return _stimuli_monitor_x_position;
}

int32_t EyeTrackerReader::GetEyeTrackerStimuliMonitorYPosition()
{
  return _stimuli_monitor_y_position;
}

int32_t EyeTrackerReader::GetEyeTrackerStimuliMonitorPixelWidth()
{
  return _stimuli_monitor_width;
}

int32_t EyeTrackerReader::GetEyeTrackerStimuliMonitorPixelHeight()
{
  return _stimuli_monitor_height;
}