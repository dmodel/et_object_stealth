#pragma once
#include "StdAfx.h"

#include "eye_tracker_data.h"
#include <cstdint>
#include <windows.h>

class EyeTrackerReader
{
public:
  static EyeTrackerReader& Instance();

  bool Aquire();
  void Release();

  bool IsAquired()
  {
    return _p_eye_tracker_memory != nullptr;
  }

  Eye_tracker_data GetEyeTrackerData()
  {
    return _eye_tracker_data;
  }
  int32_t GetEyeTrackerStimuliMonitorXPosition();
  int32_t GetEyeTrackerStimuliMonitorYPosition();
  int32_t GetEyeTrackerStimuliMonitorPixelWidth();
  int32_t GetEyeTrackerStimuliMonitorPixelHeight();

private:
  EyeTrackerReader();

  Eye_tracker_data _eye_tracker_data;
  Eye_tracker_data* _p_eye_tracker_memory;

  HANDLE _memory_mapped_file;
  HANDLE _memory_update_timer;

  HANDLE _new_eye_tracker_data_event;

  static VOID CALLBACK WaitOrTimerCallback(PVOID lpParameter, BOOLEAN);

  int32_t _stimuli_monitor_x_position;
  int32_t _stimuli_monitor_y_position;
  uint32_t _stimuli_monitor_width;
  uint32_t _stimuli_monitor_height;
};
