///////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//                           License Agreement
//
// This software is provided for evaluation purposes only. 
// Use in source and binary forms, with or without modification,
// is permitted with written consent of the copyright holder. 
//
// Redistribution of this software to third parties is strictly prohibited.
//
// This software is provided "as is" and without any express or implied warranties.
//
// By downloading, copying, installing or using the software you agree to this license.
// If you do not agree to this license, do not download, install,
// copy or use the software.
//
// To obtain a commercial license please contact the copyright holder.
//
// Copyright (C) 2013-2016, Eyediya Technologies Inc. (info@eyediya.ca)
// All rights reserved.
//
///////////////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "opencv2\opencv.hpp"
#include "EyeTrackerCore.h"
#include "ImageGrabber.h"
#include "GraphicWindow.h"
#include "eye_tracker_data.h"
#include "ET_Publisher.h"

// ****************************************************
// MAIN FLAGS AND SETTINGS (NOT INCLUDED IN SETUP FILE)
#define PARAMS_FOLDER       "../Params/"
#define DEBUG_DATA_FOLDER   "../Debug_Data/"
#define SETUP_FILE_NAME     "ET_Settings.xml"


using namespace cv;
using namespace std;

struct IMAGE_AND_TIMING
{
	cv::Mat          image;
	IMAGE_TIME_STAMP time_stamp;
	std::string      image_name;
	int              FrameNumber;
};

ET_Settings			 g_GlobalSettings;
SystemParams*		 g_pSystemParams   = NULL;
ImageGrabber*		 g_pCamWrapper     = NULL;
GraphicWindow*		 g_pDisplayWindow1 = NULL; 
GraphicWindow*		 g_pDisplayWindow2 = NULL;
ET_Publisher*        g_pET_Publisher   = NULL; 

std::vector<IMAGE_AND_TIMING> g_PROCESS_IMG_VEC;
std::vector<cv::Mat>		  g_DISPLAY_IMG_VEC;

HANDLE                  g_CAPTURE_NEXT_IMAGE;
HANDLE                  g_NEXT_IMAGE_READY;

bool    g_SlaveMousePointer    = false;
int     g_DisplayWinMode       = 2;
bool	g_EXIT_FLAG			   = false;
int     g_FrameNumber          = 0;

void   ProcessNextImage(Mat_<uchar> processImage, Mat_<uchar> displayImage, IMAGE_TIME_STAMP& ImTimeStamp, Eye_tracker_data& ET_Data);
bool   InitAll(std::string SetupFileName);
void   DeallocateAll();
void   CaptureImagesAsync(void* IMG);
void   HandleInputs(Mat_<uchar> processImage, Mat_<uchar> displayImage);


// main
int main(int argc, char* argv[])
{
	SetPriorityClass(GetCurrentProcess(), ABOVE_NORMAL_PRIORITY_CLASS);

	String SetupFileName = string(PARAMS_FOLDER) + string(SETUP_FILE_NAME);

	if ( !InitAll(SetupFileName) )
		return 0;

	// launch image capture
	g_CAPTURE_NEXT_IMAGE = CreateEvent(NULL, true, false, NULL);
	g_NEXT_IMAGE_READY   = CreateEvent(NULL, true, false, NULL);

	HANDLE hThread = (HANDLE)_beginthread(CaptureImagesAsync, 0, NULL);
	SetThreadPriority(hThread, THREAD_PRIORITY_HIGHEST);
	SetEvent(g_CAPTURE_NEXT_IMAGE);

	// main loop
	while ( !g_EXIT_FLAG ) 
	{
		// wait the next image
		WaitForSingleObject(g_NEXT_IMAGE_READY, INFINITE);
		
		Mat_<uchar>       processImage, displayImage;
		IMAGE_AND_TIMING  CUR_IMAGE;
		IMAGE_TIME_STAMP  cur_image_time_stamp;

		if (g_PROCESS_IMG_VEC.size())
		{
			CUR_IMAGE    = g_PROCESS_IMG_VEC.front();
			g_PROCESS_IMG_VEC.erase( g_PROCESS_IMG_VEC.begin() );
		
			processImage         = CUR_IMAGE.image;
			cur_image_time_stamp = CUR_IMAGE.time_stamp;

			displayImage = g_DISPLAY_IMG_VEC.front();           
			g_DISPLAY_IMG_VEC.erase( g_DISPLAY_IMG_VEC.begin() );
		}
		else
			OutputDebugStringA("FRONT END: Main(): g_PROCESS_IMG_VEC is empty!!!\n");

		ResetEvent(g_NEXT_IMAGE_READY);
		SetEvent(g_CAPTURE_NEXT_IMAGE);

		// process the next image
		if (!g_EXIT_FLAG)
		{
			Eye_tracker_data ET_Data;
			ProcessNextImage(processImage, displayImage, cur_image_time_stamp, ET_Data);

			// publish results
			ET_Data.frame_number = CUR_IMAGE.FrameNumber;
			g_pET_Publisher->ET_Publish(ET_Data);
		}
		else
			break;
		
		// verify that eye-tracker is not exiting
		if (g_pDisplayWindow1->GetExitFlag() || g_pDisplayWindow2->GetExitFlag())
			g_EXIT_FLAG = true;

		if (g_EXIT_FLAG)
		{
			OutputDebugStringA("FRONT END: Main(): Received g_EXIT_FLAG - Breaking...\n");
			break;
		}

		// Handle user inputs
		HandleInputs(processImage, displayImage);
	}

	OutputDebugStringA("FRONT END: Main(): Setting g_CAPTURE_NEXT_IMAGE\n");
	SetEvent(g_CAPTURE_NEXT_IMAGE);

	DeallocateAll();

	return 0;
}


void   ProcessNextImage(Mat_<uchar> processImage, Mat_<uchar> displayImage, IMAGE_TIME_STAMP& ImTimeStamp, Eye_tracker_data& ET_Data)
{
	ProcessNextImageCore(processImage, displayImage, ImTimeStamp, g_pSystemParams, ET_Data);
	
	// ***********************************************************
	// Update mouse pointer position	
	if (g_SlaveMousePointer)
	{
		if (ET_Data.right_eye_point_of_gaze_estimate.x_in_pixels != -1)
		{
			Point2i pog_px(ET_Data.right_eye_point_of_gaze_estimate.x_in_pixels, ET_Data.right_eye_point_of_gaze_estimate.y_in_pixels);
			SetCursorPos(pog_px.x + g_pSystemParams->GetDisplayPlanePtr()->get_monitor_offset_x_px(), pog_px.y + g_pSystemParams->GetDisplayPlanePtr()->get_monitor_offset_y_px());
		}
	}
			
	// ***********************************************************
	// Update display results
	double startTimeDiplay = (double)getTickCount();
	if (g_DisplayWinMode == 1)
	{
		unsigned int size_x = g_pDisplayWindow1->GetWidth();
		unsigned int size_y = g_pDisplayWindow1->GetHeight();
		Mat          ResizedDisplayMat(size_y, size_x, CV_8U);

		OutputDebugStringA("FRONT END: Starting Image Resizing \n");
		resize(processImage, ResizedDisplayMat, ResizedDisplayMat.size(), 0, 0, INTER_NEAREST);
		OutputDebugStringA("FRONT END: Finished Image Resizing \n");
		g_pDisplayWindow1->displayImage(&ResizedDisplayMat);
		OutputDebugStringA("FRONT END: Done Displaying resized Image \n");
	}
	else if (g_DisplayWinMode == 2)
	{
		g_pDisplayWindow2->displayImage(&displayImage);
	}
}

bool   InitAll(string SetupFileName)
{
	FileStorage fs(SetupFileName, FileStorage::READ); // Read the settings
    if (!fs.isOpened())
    {
        cout << "Could not open the configuration file: \"" << SetupFileName << "\". Press any key to exit..." << endl;
		_getch();
        return false;
    }
    fs["Settings"] >> g_GlobalSettings;
    fs.release();                                         // close Settings file

    if (!g_GlobalSettings.goodInput)
    {
        cout << "Invalid input detected. Application stopping.  Press any key to exit..." << endl;
		_getch();
        return false;
    }

	FreeConsole();

	g_pSystemParams	  = new SystemParams( string(PARAMS_FOLDER) + g_GlobalSettings.SYSPARAMS_FILENAME, 0, 0, g_GlobalSettings.N_display );

	if (!InitCore(g_GlobalSettings, g_pSystemParams, PARAMS_FOLDER))
		return false;

	g_pET_Publisher = new ET_Publisher();

	g_pDisplayWindow1 = new GraphicWindow("Eye Tracker", CV_WINDOW_AUTOSIZE); //CV_WINDOW_NORMAL|CV_WINDOW_KEEPRATIO
	g_pDisplayWindow2 = new GraphicWindow("Eye Tracker - Full Screen", CV_WINDOW_AUTOSIZE); //CV_WINDOW_NORMAL|CV_WINDOW_KEEPRATIO
	
	g_DisplayWinMode  = 1;

	// configure position of Diplay Mode 1
	unsigned int width_px  = g_pSystemParams->GetDisplayPlanePtr()->getWidth_px();
	unsigned int height_px = g_pSystemParams->GetDisplayPlanePtr()->getHeight_px();

	unsigned int size_x = 160;
	unsigned int size_y = 90;

	g_pDisplayWindow1->resizeGraphicWindow(size_x, size_y);
	g_pDisplayWindow1->SetPosition(width_px/2 - size_x - 10, height_px/2 - size_y - 250);
	g_pDisplayWindow1->SetTopMost();

	// configure position of Diplay Mode 2 & 3
	g_pDisplayWindow2->SetPosition(0, 0);

	g_pDisplayWindow1->HideWindow();
	g_pDisplayWindow2->HideWindow();

	OutputDebugStringA("FRONT END: Initialization done. \n");

	return true;
}

void   DeallocateAll()
{
	Sleep(1000);

	delete g_pET_Publisher;
	delete g_pCamWrapper;
	delete g_pDisplayWindow1;
	delete g_pDisplayWindow2;
	delete g_pSystemParams;

	CloseHandle(g_CAPTURE_NEXT_IMAGE);
	CloseHandle(g_NEXT_IMAGE_READY);
}

void   CaptureImagesAsync(void* IMG)
{
	double tickFreq  = getTickFrequency();

	CAM_TYPE CameraType = (CAM_TYPE)g_GlobalSettings.ImageGrabberType; 

	if (CameraType != CAM_TYPE::IMAGE)
	{
		float FrameRate = g_GlobalSettings.FrameRate;
		Size  SenSize   = g_pSystemParams->GetCameraPtr()->getSensorSize(); 
		g_pCamWrapper   = new ImageGrabber(CameraType, SenSize, FrameRate); 
	}
	
	Mat rawImage, newImage;
	int  CntFrameSameImage = 0;
	bool bLastImage        = false;
	while (!g_EXIT_FLAG ) 
	{
		double beforeTriggerTime = (double)getTickCount();

		WaitForSingleObject(g_CAPTURE_NEXT_IMAGE, INFINITE);
		ResetEvent(g_CAPTURE_NEXT_IMAGE);

		double startTime = (double)getTickCount();

		char   buf[1000];
		sprintf_s(buf, "FRONT END: CaptureImagesAsync(): Image Acquisition - trigger wait time is %f ms \n", (startTime - beforeTriggerTime)/tickFreq*1000);
		OutputDebugStringA(buf);


		while (!g_pCamWrapper->captureImage(rawImage))
			Sleep(1);


		// save image capture time stamp
		IMAGE_AND_TIMING CUR_IMAGE; 
		CUR_IMAGE.time_stamp.TICK_COUNT = (double)getTickCount();
		GetSystemTime(&CUR_IMAGE.time_stamp.SYS_TIME);
		CUR_IMAGE.FrameNumber = g_FrameNumber++;

		if (rawImage.channels() == 3)
		{

			if (newImage.rows != rawImage.rows || newImage.cols != rawImage.cols)
				newImage = Mat(rawImage.size(), CV_8U);

			cvtColor(rawImage, newImage, CV_BGR2GRAY);

		}
		else
			newImage = rawImage;


		if (newImage.rows > 0 && newImage.cols > 0)
		{
			Mat dispImage;
			newImage.copyTo(dispImage);
			newImage.copyTo(CUR_IMAGE.image);

			CUR_IMAGE.image_name = string("RealTimeImage");

			g_DISPLAY_IMG_VEC.push_back(dispImage);
			g_PROCESS_IMG_VEC.push_back(CUR_IMAGE);
		}

		SetEvent(g_NEXT_IMAGE_READY);

		double endTime = (double)getTickCount();

		sprintf_s(buf, "FRONT END: CaptureImagesAsync(): Image Acquisition time is %f ms \n", (endTime - startTime)/tickFreq*1000);
		OutputDebugStringA(buf);

	}

	OutputDebugStringA("FRONT END: CaptureImagesAsync(): Exiting\n");

	SetEvent(g_NEXT_IMAGE_READY);
	OutputDebugStringA("FRONT END: CaptureImagesAsync(): After SetEvent(g_NEXT_IMAGE_READY);\n");


	_endthread();
	OutputDebugStringA("FRONT END: CaptureImagesAsync(): After _endthread();\n");
}

void   HandleInputs(Mat_<uchar> processImage, Mat_<uchar> displayImage)
{
	double tickFreq       = getTickFrequency();
	double startTime_Keys = (double)getTickCount();
	UINT   file_num       = (UINT)(startTime_Keys/tickFreq*1000);

	int    inKey   = 0;
	inKey = waitKey(1); // refresh OpenCV windows

	HandleInputsCore(inKey, g_GlobalSettings, PARAMS_FOLDER);

	if (inKey == VK_ESCAPE)
	{
		g_EXIT_FLAG = true; //EXIT
	}
	else if (inKey == VK_BACK)
	{
		if (g_DisplayWinMode == 1)
			g_pDisplayWindow1->SetFocus();
		else if (g_DisplayWinMode == 2)
			g_pDisplayWindow2->SetFocus();
	}
	else if (inKey == 's' || inKey == 'S')
	{
		char buf[100];
		sprintf_s(buf, "%s/Images/image_%d_original.tiff", DEBUG_DATA_FOLDER, file_num);
		imwrite(buf, processImage);
	}
	else if (inKey == '1')
	{
		g_DisplayWinMode = 1;
		g_pDisplayWindow1->HideWindow();
		g_pDisplayWindow2->HideWindow();
		g_pDisplayWindow1->ShowWindow();
	}
	else if (inKey == '2')
	{
		g_DisplayWinMode = 2;
		g_pDisplayWindow1->HideWindow();
		g_pDisplayWindow2->HideWindow();
		g_pDisplayWindow2->ShowWindow();
	}

	else if (inKey == '+' || inKey == '=')
		g_SlaveMousePointer = !g_SlaveMousePointer;

	double endtTime_Keys  = (double)getTickCount();
	double KeysTime = (endtTime_Keys - startTime_Keys)/tickFreq*1000;
}