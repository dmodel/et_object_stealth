///////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//                           License Agreement
//
// This software is provided for evaluation purposes only. 
// Use in source and binary forms, with or without modification,
// is permitted with written consent of the copyright holder. 
//
// Redistribution of this software to third parties is strictly prohibited.
//
// This software is provided "as is" and without any express or implied warranties.
//
// By downloading, copying, installing or using the software you agree to this license.
// If you do not agree to this license, do not download, install,
// copy or use the software.
//
// To obtain a commercial license please contact the copyright holder.
//
// Copyright (C) 2013-2016, Eyediya Technologies Inc. (info@eyediya.ca)
// All rights reserved.
//
///////////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "ET_Publisher.h"

using namespace cv;
using namespace std;

#define SHARED_MEMORY_FILE_NAME "MyFileMappingObject"

const int SHARED_MEMORY_SIZE = 1024;


ET_Publisher::ET_Publisher(void) 
{
	// Allocate File-mapping object
	do
	{
		m_HFileMapping = ::CreateFileMappingA ((HANDLE)0xFFFFFFFF,	// special handle to let the 
																	// the operating system know 
																	// to use system paging file.
			(LPSECURITY_ATTRIBUTES) NULL, 						    //default security
			PAGE_READWRITE, 									    //permit read-write access
			0,         											    //high-order 32 bits  of max size 0
			SHARED_MEMORY_SIZE*sizeof(double),						//low-order 32 bits of max size - 1K
			SHARED_MEMORY_FILE_NAME);								//name of file mapping object

		// TODO - add option to kill other eye-tracking processes
		if (m_HFileMapping == NULL)									//If error encountered in allocating memory
		{
			MessageBoxA(0,"Make Sure Another Instant of Eye-Tracker Is Not Running!",
				"Initialization Failure",MB_OK|MB_ICONERROR);
		}

	}
	while (m_HFileMapping == NULL);

	// Treat allocated memory as array of doubles	
	m_pPtrSharedMemory = (double *)::MapViewOfFile(m_HFileMapping, FILE_MAP_ALL_ACCESS, 0, 0, SHARED_MEMORY_SIZE*sizeof(double));
		
	// If error encountered in mapping common memory space
	if(m_pPtrSharedMemory == NULL)
	{
		MessageBoxA(0,"Failed to map shared memory!",
			"Shared Memory Mapping Error",MB_OK|MB_ICONERROR);
	}


	// set shared memory to zero
	for (int n = 0; n < SHARED_MEMORY_SIZE; n++)
	{
		m_pPtrSharedMemory[n] = 0;
	}
	

}

/////////////////////////////////////////////////////////////////////////////////

ET_Publisher::~ET_Publisher(void) 
{
	UnmapViewOfFile (m_pPtrSharedMemory);
	m_pPtrSharedMemory = NULL;
	CloseHandle (m_HFileMapping);
}


void ET_Publisher::ET_Publish(Eye_tracker_data& ET_Data)
{
	memcpy(m_pPtrSharedMemory, &ET_Data, sizeof(ET_Data));
}



