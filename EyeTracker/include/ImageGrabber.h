///////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//                           License Agreement
//
// This software is provided for evaluation purposes only. 
// Use in source and binary forms, with or without modification,
// are permitted with written consent of the copyright holder. 
//
// Redistribution of this software to third parties is strictly prohibited.
//
// This software is provided "as is" and without any express or implied warranties.
//
// By downloading, copying, installing or using the software you agree to this license.
// If you do not agree to this license, do not download, install,
// copy or use the software.
//
// To obtain a commercial license please contact the copyright holder.
//
// Copyright (C) 2013-2016, Eyediya Technologies Inc. (info@eyediya.ca)
// All rights reserved.
//
///////////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "opencv2\opencv.hpp"

#ifdef  OPENCVCAMERA_EXPORTS
#define OPENCVCAMERA_API __declspec(dllexport) 
#else
#define OPENCVCAMERA_API __declspec(dllimport) 
#endif

enum CAM_TYPE {IMAGE = 0, OpenCV = 1, DirectShow = 2, PT_GREY = 3};

class FrameGrabber
{
public:
	FrameGrabber(){};
	~FrameGrabber(){};
	
	virtual bool isOpened()           = 0;
	virtual bool read(cv::Mat& Frame) = 0;
	virtual bool release()            = 0;
};

class OPENCVCAMERA_API ImageGrabber
{
public:
	ImageGrabber(CAM_TYPE  CamType = DirectShow, cv::Size SensorSize = cv::Size(1280, 720), float FrameRate = 30, int DEV_ID = 1);
	~ImageGrabber();
	bool captureImage(cv::Mat& outMat);
	bool isOpened();

private:
	FrameGrabber* m_FrameGrabber;

};