///////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//                           License Agreement
//
// This software is provided for evaluation purposes only. 
// Use in source and binary forms, with or without modification,
// is permitted with written consent of the copyright holder. 
//
// Redistribution of this software to third parties is strictly prohibited.
//
// This software is provided "as is" and without any express or implied warranties.
//
// By downloading, copying, installing or using the software you agree to this license.
// If you do not agree to this license, do not download, install,
// copy or use the software.
//
// To obtain a commercial license please contact the copyright holder.
//
// Copyright (C) 2013-2016, Eyediya Technologies Inc. (info@eyediya.ca)
// All rights reserved.
//
///////////////////////////////////////////////////////////////////////////////////////
#pragma once

#include "stdafx.h"

#include "opencv2\opencv.hpp"
#include <iostream>
#include <sstream>
#include <time.h>

class ET_Settings
{
public:
    ET_Settings() : goodInput(false) {}

    void read(const cv::FileNode& node)                          //Read serialization for this class
    {
#ifdef _WINDOWS
		node["N_Display"]			>> N_display;
#else
		N_display = 1;
#endif

        node["ImageGrabberType"] >> ImageGrabberType;
        node["FrameRate"]        >> FrameRate;

        node["SYSPARAMS_FILENAME"] >> SYSPARAMS_FILENAME;
        node["DEFAULT_USER_NAME"]  >> DEFAULT_USER_NAME;

		node["Focal_Length_mm"]  >> Focal_Length_mm;
        node["LS_Dist_cm"]       >> LS_Dist_cm;
        node["PT_GREY_Flag"]     >> PT_GREY_Flag;

        interprate();
    }

    void interprate()
    {
        goodInput = true;
        if (N_display < 1 || Focal_Length_mm <= 0 || LS_Dist_cm <= 0 || FrameRate <= 0 ||
            ImageGrabberType < 0)
        {
			std::cerr << "Invalid input" << std::endl;
            std::cerr << "Focal_Length_mm: " << Focal_Length_mm << std::endl;
            std::cerr << "LS_Dist_cm: " << LS_Dist_cm << std::endl;
            goodInput = false;
        }
    }

public:
    // appearance and graphics
	int   N_display;

    // image capture controls
	int   ImageGrabberType;
	float FrameRate;

	// setup file names
	std::string  SYSPARAMS_FILENAME;
	std::string  DEFAULT_USER_NAME;

	// System Setup
    float   Focal_Length_mm;
	float   LS_Dist_cm;
	bool  PT_GREY_Flag;

	// valid input indicator
	bool  goodInput;
};


void read(const cv::FileNode& node, ET_Settings& x, const ET_Settings& default_value = ET_Settings())
{
    if (node.empty())
        x = default_value;
    else
        x.read(node);
}
