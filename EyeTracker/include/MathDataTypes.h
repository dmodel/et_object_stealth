///////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//                           License Agreement
//
// This software is provided for evaluation purposes only. 
// Use in source and binary forms, with or without modification,
// is permitted with written consent of the copyright holder. 
//
// Redistribution of this software to third parties is strictly prohibited.
//
// This software is provided "as is" and without any express or implied warranties.
//
// By downloading, copying, installing or using the software you agree to this license.
// If you do not agree to this license, do not download, install,
// copy or use the software.
//
// To obtain a commercial license please contact the copyright holder.
//
// Copyright (C) 2013-2016, Eyediya Technologies Inc. (info@eyediya.ca)
// All rights reserved.
//
///////////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "opencv2\opencv.hpp"
#include "NumericalPrecision.h"

#ifdef USE_DOUBLE_PRECISION
	typedef double Real;
	#define CV_F   CV_64F
	#define CV_FC2 CV_64FC2 // OpenCV 2-channel double precision floating point type 
	#define _dot ddot // Use ddot for double precision calculation of the dot product
	#define REAL_MAX DBL_MAX
	// IMPORTANT: All these defines must be consistent with the above typedef (floating-point type) Real
#else
	typedef float Real;
	#define CV_F   CV_32F
	#define CV_FC2 CV_32FC2 // OpenCV 2-channel single precision floating point type 
	#define _dot dot // Use dot for single precision calculation of the dot product
	#define REAL_MAX FLT_MAX
	// IMPORTANT: All these defines must be consistent with the above typedef (floating-point type) Real
#endif

typedef cv::Point_<Real> Point2D;
typedef cv::Point3_<Real> Point3D;
typedef cv::Vec<Real, 2> Vec2D;
typedef cv::Vec<Real, 3> Vec3D;
typedef cv::Vec<Real, 8> Vec8D;
typedef cv::Matx<Real, 3, 3> Matx3x3;


