///////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//                           License Agreement
//
// This software is provided for evaluation purposes only. 
// Use in source and binary forms, with or without modification,
// is permitted with written consent of the copyright holder. 
//
// Redistribution of this software to third parties is strictly prohibited.
//
// This software is provided "as is" and without any express or implied warranties.
//
// By downloading, copying, installing or using the software you agree to this license.
// If you do not agree to this license, do not download, install,
// copy or use the software.
//
// To obtain a commercial license please contact the copyright holder.
//
// Copyright (C) 2013-2016, Eyediya Technologies Inc. (info@eyediya.ca)
// All rights reserved.
//
///////////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "StdAfx.h"
#include "opencv2\opencv.hpp"
#include "eye_tracker_data.h"


class ET_Publisher {
public:
	ET_Publisher(void);
	~ET_Publisher(void);

	void ET_Publish(Eye_tracker_data& ET_Data);

private:
	HANDLE  m_HFileMapping; 		// identifier for File-mapping object
	double* m_pPtrSharedMemory; 	// pointer to shared memory
	HANDLE  m_HMappingForFile; 		

};


