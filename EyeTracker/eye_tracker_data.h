#pragma once

struct Eye_tracker_data
{
  double system_type;
  double operating_mode;
  double frame_number;
  struct
  {
    double year;
    double month;
    double day;
    double hour;
    double minute;
    double second;
    double millisecond;
  } timestamp;

  double state;
  double padding1[9];

  double scene_camera;
  double scene_camera_flag;
  double padding2[14];

  double screen_offset_x_in_mm;
  double screen_offset_y_in_mm;
  double screen_offset_z_in_mm;
  double visual_stimuli_monitor_index;
  double screen_width_in_pixels;
  double screen_height_in_pixels;
  double screen_width_in_mm;
  double screen_height_in_mm;

  double calibration_target_x_in_mm;
  double calibration_target_y_in_mm;
  double calibration_target_z_in_mm;
  double calibration_target_is_shown;
  double padding3[2];

  double num_cameras;
  double num_light_sources;
  double num_eyes;
  double padding4[7];

  struct
  {
    double mil_timestamp;
    double mil_buffer_index;
    double time_at_entry_to_grab_end_hook_router;
    double camera_timestamp;
    double camera_frame_count;
    double corrected_camera_timestamp;
    double corrected_camera_frame_count;
    double padding[13];
  } timing_and_counting[2];

  struct
  {
    struct
    {
      double eye_found;
      double pupil_x_pos_in_pixels;
      double pupil_y_pos_in_pixels;
      double pupil_area_in_mm_square;
      double pupil_ellipse_box_width_in_pixels;
      double pupil_ellipse_box_height_in_pixels;
      double pupil_ellipse_angle_in_degrees;
      double padding[3];

      struct
      {
        double cr_x_in_pixels;
        double cr_y_in_pixels;
        double padding[3];
      } corneal_reflection[4];
      double padding1[10];

      double horizontal_eye_angle;
      double vertical_eye_angle;
      double padding2[5];

      double occluder_target_center_x;
      double occluder_target_center_y;
      double padding3;
    } camera[2];
  } right_eye, left_eye;

  struct
  {
    double x_in_pixels;
    double y_in_pixels;
    double x_in_mm;
    double y_in_mm;
    double z_in_mm;
    struct
    {
      double x;
      double y;
      double z;
    } d_vector, c_vector, p_vector, optical_axis_vector, visual_axis_vector;
    double theta_eye_in_degrees;
    double phi_eye_in_degrees;
	double success;
    double padding[27];
  } right_eye_point_of_gaze_estimate, left_eye_point_of_gaze_estimate;

  struct
  {
    double success;
    double theta_rad;
    double phi_rad;
    double kappa_rad;
    double x_min_in_mm;
    double x_max_in_mm;
    double y_min_in_mm;
    double y_max_in_mm;
    double z_min_in_mm;
    double z_max_in_mm;
  } head_pose_estimate;

  double padding5[613];
};
