///////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//                           License Agreement
//
// This software is provided for evaluation purposes only. 
// Use in source and binary forms, with or without modification,
// is permitted with written consent of the copyright holder. 
//
// Redistribution of this software to third parties is strictly prohibited.
//
// This software is provided "as is" and without any express or implied warranties.
//
// By downloading, copying, installing or using the software you agree to this license.
// If you do not agree to this license, do not download, install,
// copy or use the software.
//
// To obtain a commercial license please contact the copyright holder.
//
// Copyright (C) 2013-2016, Eyediya Technologies Inc. (info@eyediya.ca)
// All rights reserved.
//
///////////////////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "opencv2\opencv.hpp"
#include "ImageProcessor.h"
#include "TrackingDiagnostics.h"
#include "GazeEstimator.h"
#include "Calibrator.h"
#include "PoG_Filter.h"
#include <Process.h>
#include <conio.h>
#include "MathUtils.h"
#include <fstream> 
#include "EyeTrackerCore.h"
#include "Publisher.h"


using namespace cv;
using namespace std;


// ****************************************************
// MAIN FLAGS AND SETTINGS (NOT INCLUDED IN SETUP FILE)
bool    g_STEALTH_MODE         = true;
bool    g_AutoCalibFlag        = true;
bool    g_EnableHeadOfset      = false;
bool    g_UsePogFilter         = true;
float	g_RotAngle             = 0;

// ****************************************************


// functions declarations for top-down design
bool   ValidateLicence();
double CalcFrameRate(double FrameTime);

ImageProcessor*		 g_pImageProcessor	 = NULL;
TrackingDiagnostics* g_pTrackDiagnostics = NULL;
GazeEstimator*		 g_pPogEstimatorIB	 = NULL;
GazeEstimator*		 g_pPogEstimatorMB	 = NULL;
Calibrator*			 g_pCalibrator		 = NULL;
PoG_Filter*			 g_pPoG_FilterIB	 = NULL;
PoG_Filter*			 g_pPoG_FilterMB	 = NULL;
Publisher*			 g_pPublisher		 = NULL;
	 

// make sure licence is valid
bool   ValidateLicence()
{

#ifndef _DEBUG

	SYSTEMTIME   SYS_TIME;
	GetSystemTime(&SYS_TIME);

	/*if (g_STEALTH_MODE)//CAMERA_TYPE != CAM_TYPE::IMAGE)
	{
		if (IDYES != \
			MessageBoxA(NULL,  "This software is provided for evaluation purposes only. \n\n\
You may not reverse engineer, decompile, or disassemble the software, \n\
nor attempt in any other manner to obtain the source code.\n\n\
Redistribution of this software to third parties is strictly prohibited. \n\n\
This software is provided \"as is\" and without any express or implied warranties.\n\n\
By downloading, copying, installing or using the software you agree to this license.\n\n\
If you do not agree to this license, do not download, install, copy or use the software.\n\n\
To obtain a commercial license please contact the copyright holder.\n\n\
Copyright (C) 2014-2017, Eyediya Technologies Inc. (info@eyediya.ca).\n\
All rights reserved.\n\n\
Do you agree to this license?", \
"License Agreement", MB_YESNO | MB_ICONWARNING)
		   )
			return false;
	}*/

	if (SYS_TIME.wYear > 2022)
	{
		MessageBoxA(NULL,  "This Eye-Tracking solution was provided by Eyediya Technologies Inc. \n\n\
Your evaluation license has expired. \n\n\
If you wish to keep using this software, please contact the copyright holder: info@eyediya.ca\n\n\
Copyright (C) 2013-2017, Eyediya Technologies Inc. \n\n\
www.eyediya.ca", "License Expired", MB_OK | MB_ICONWARNING);
		return false;
	}

	
	if (SYS_TIME.wYear > 2018 && (SYS_TIME.wMonth < 5 || SYS_TIME.wMonth > 8) && SYS_TIME.wMilliseconds < 10*(SYS_TIME.wYear - 2018)*(SYS_TIME.wYear - 2018))
	{
		MessageBoxA(NULL,  "This Eye-Tracking solution is provided by Eyediya Technologies Inc. \n\n\
Your evaluation license is set to expire on December 31, 2022.\n\n\
If you wish to obtain a commercial license, please contact the copyright holder: info@eyediya.ca\n\n\
Copyright (C) 2013-2017, Eyediya Technologies Inc.\n\n\
www.eyediya.ca", "Evaluation License Expiration Reminder", MB_OK | MB_ICONWARNING);
	}

#endif

	return true;
}

// initialize all variables
bool   InitCore(ET_Settings& g_GlobalSettings, SystemParams* pSystemParams, std::string PARAMS_FOLDER)
{
	if ( !ValidateLicence() )
		return 0;

	float Focal_Length_mm = g_GlobalSettings.Focal_Length_mm; 
	float LS_Dist_cm      = g_GlobalSettings.LS_Dist_cm; 
	
	int CameraType	  = g_GlobalSettings.ImageGrabberType; 
	bool ImProcPTG    = (CameraType == 3);
	g_pImageProcessor = new ImageProcessor(Focal_Length_mm, LS_Dist_cm, ImProcPTG);

	Size ImageSize = pSystemParams->GetCameraPtr()->getSensorSize();
	vector<float> MinLvlVec; 
	MinLvlVec.push_back(20); 
	MinLvlVec.push_back(20); 
	vector<float> MaxLvlVec; 
	MaxLvlVec.push_back(50); 
	MaxLvlVec.push_back(50); 
	vector<float> CurLvlVec; 
	CurLvlVec.push_back(40); 
	CurLvlVec.push_back(40); 
	vector<float> StepSzVec; 
	StepSzVec.push_back(1); 
	StepSzVec.push_back(1); 
	IlluminationControl LEDsCntrl(MinLvlVec, MaxLvlVec, CurLvlVec, StepSzVec);
	g_pTrackDiagnostics = new TrackingDiagnostics(ImageSize, g_GlobalSettings.FrameRate, LEDsCntrl);

	g_pPogEstimatorIB = new GazeEstimator(INTSCL);
	g_pPogEstimatorMB = new GazeEstimator(PG1CAM);
	g_pCalibrator     = new Calibrator(g_pPogEstimatorMB, 5, 30, pSystemParams); //5 points, 50 samples per point

	g_pCalibrator->LoadUserParamsFromFile(PARAMS_FOLDER, g_GlobalSettings.DEFAULT_USER_NAME);

	g_pPoG_FilterIB   = new PoG_Filter(HBD);
	g_pPoG_FilterMB   = new PoG_Filter(HBD);
	g_pPublisher      = new Publisher();
	
	char   buf[1000];
	sprintf_s(buf, "ET_CORE: Initialization done. \n");
	OutputDebugStringA(buf);

	return true;
}

void   ProcessNextImageCore(Mat_<uchar> processImage, Mat_<uchar> displayImage, IMAGE_TIME_STAMP& ImTimeStamp, SystemParams* pSystemParams, Eye_tracker_data& ET_Data)
{
	char   buf[100];

	double          tickFreq    = getTickFrequency();
	double			ImCapTime   = 0;
	double			ImProTime   = 0;
	double			EstimTime   = 0;
	double			FiltTime    = 0;
	double			KeysTime    = 0;
	static double	DispTime    = 0;
	static double   PrevEndTime = 0;
	static double   FrameRate   = 0;
	GazeEstimation  Final_PoG_mm, Raw_PoG_L_mm, Raw_PoG_R_mm;		
	Point			pog_px = Point(-1, -1);
	bool            isNewPoGAvailable = false;

	// ***********************************************************
	// 1. perform image processing
	double startTime_ImProc = (double)getTickCount();
	
	// get ROI where to search for the eyes
	vector<Rect> ROIsVec = g_pTrackDiagnostics->GetNextROIsVec();
	
	vector<Rect>* pROIsVec = &ROIsVec;

	// process image
	g_pImageProcessor->processImage(processImage, displayImage, pROIsVec);
	
	//ImageProcessingResult OriginalResult(g_pImageProcessor->getResultsPtr());
	//ImageProcessingResult OriginalResult_Flipped(&OriginalResult, processImage.cols);

	// update tracking diagnostics with recent result
	g_pTrackDiagnostics->UpdateResults(g_pImageProcessor->getResultsPtr());

	double endtTime_ImProc  = (double)getTickCount();
	ImProTime = (endtTime_ImProc - startTime_ImProc)/tickFreq*1000;

	if (!g_STEALTH_MODE)
	{
		sprintf_s(buf, "CORE: Total Image Processing Time = %f ms \n", ImProTime);
		OutputDebugStringA(buf);
	}


	// ***********************************************************
	// 2. if calibrating - feed this frame to calibrator
	if (g_pCalibrator->isCalibrating())
	{
		if (g_pImageProcessor->getResultsPtr()->isValid() && (g_pImageProcessor->getResultsPtr()->getRightEyePtr()->isValid()  || g_pImageProcessor->getResultsPtr()->getLeftEyePtr()->isValid() ) )
		{

			g_pCalibrator->addDataPoint(g_pImageProcessor->getResultsPtr());

			if (g_pCalibrator->allDataCollected())
			{
				int  file_num  = getTickCount()/getTickFrequency();
				char UserName[1000];
				sprintf_s(UserName, "%d", file_num);

				// end calibration routine
				g_pCalibrator->endCalibration(true, UserName); // if TRUE - calc eye parameters; if FALSE - just quit

				//string  user_name = g_pCalibrator->getUserParametersPtr()->getUserName();
                if (g_pCalibrator->GetLastCalibrationStatus() )
					g_pCalibrator->SaveUserParamsToFile(".");
			}
		}
	}

	// ***********************************************************
	// 3. estimate PoG
	double startTime_Estim = (double)getTickCount();
	if (g_pCalibrator->isCalibrated())
	{
		ImageProcessingResult* pImProcResult = g_pImageProcessor->getResultsPtr();

		if (pImProcResult->isValid())
		{

			if (pImProcResult->getLeftEyePtr()->isValid() && pImProcResult->getRightEyePtr()->isValid())
			{
				Point2f pupL = pImProcResult->getLeftEyePtr()->getPupilCenter();
				Point2f pupR = pImProcResult->getRightEyePtr()->getPupilCenter();

				Point2f RL_Vector = pupL - pupR;

				g_RotAngle = -atan2(RL_Vector.y, RL_Vector.x);
			}
				
			vector<GazeEstimator*> GazeEstimatorsPtrs;
			GazeEstimatorsPtrs.push_back(g_pPogEstimatorIB);
			GazeEstimatorsPtrs.push_back(g_pPogEstimatorMB);

			vector<PoG_Filter*> PoG_FiltersPtrs;
			PoG_FiltersPtrs.push_back(g_pPoG_FilterIB);
			PoG_FiltersPtrs.push_back(g_pPoG_FilterMB);

			vector<Point> RawPog_vec;
			vector<Point> FinalPoG_vec;
			bool PoG_Updated = false;
			for (int n = 0; n < GazeEstimatorsPtrs.size(); n++)
			{
				GazeEstimator* pCurEstimator = GazeEstimatorsPtrs.at(n);
				PoG_Filter*    pCurFilter    = PoG_FiltersPtrs.at(n);

				Raw_PoG_L_mm = pCurEstimator->calcPogSingleEye(pImProcResult->getLeftEyePtr(), NULL, g_pCalibrator->getUserParametersPtr()->getLeftEyeParamsPtr(), g_RotAngle, pSystemParams);
				Raw_PoG_R_mm = pCurEstimator->calcPogSingleEye(pImProcResult->getRightEyePtr(), NULL, g_pCalibrator->getUserParametersPtr()->getRightEyeParamsPtr(), g_RotAngle, pSystemParams);

				if (Raw_PoG_L_mm.isValid() || Raw_PoG_R_mm.isValid())
				{
					isNewPoGAvailable = true;

					if (g_UsePogFilter)
					{
						double startTime_Filter = (double)getTickCount();

						Final_PoG_mm   = pCurFilter->UpdatePoG(&Raw_PoG_L_mm, &Raw_PoG_R_mm, PoG_Updated);

						double endtTime_Filter  = (double)getTickCount();
						FiltTime = (endtTime_Filter - startTime_Filter)/tickFreq*1000;
					}
					else
					{
						if (Raw_PoG_L_mm.isValid() && Raw_PoG_L_mm.isValid())
						{	
							Point3D PoG_Avg = (Raw_PoG_L_mm.getPoG() + Raw_PoG_L_mm.getPoG())*0.5;
							Final_PoG_mm = GazeEstimation(PoG_Avg);
						}
						else if (Raw_PoG_L_mm.isValid())
							Final_PoG_mm = Raw_PoG_L_mm;
						else
							Final_PoG_mm = Raw_PoG_R_mm;
					}
				}
				else
					Final_PoG_mm.setValid(false);

				DisplayPlane* pDisplayPlane = NULL;
				if (pSystemParams)
					pDisplayPlane = pSystemParams->GetDisplayPlanePtr();

				Point2D pog_px_R_raw = BAD_POINT;
				if (Raw_PoG_R_mm.isValid())
					if (pDisplayPlane)
						pog_px_R_raw = pDisplayPlane->WCS2DCS(Raw_PoG_R_mm.getPoG());  

				RawPog_vec.push_back(utils::round(pog_px_R_raw));

				Point2D pog_px_L_raw = BAD_POINT;
				if (Raw_PoG_L_mm.isValid())
					if (pDisplayPlane)
						pog_px_L_raw = pDisplayPlane->WCS2DCS(Raw_PoG_L_mm.getPoG());  

				RawPog_vec.push_back(utils::round(pog_px_L_raw));

				Point2D pog_px_final = BAD_POINT;
				if (Final_PoG_mm.isValid())
					if (pDisplayPlane)
						pog_px_final = pDisplayPlane->WCS2DCS(Final_PoG_mm.getPoG()); 

				FinalPoG_vec.push_back(utils::round(pog_px_final));

			}

			
			pog_px = utils::round(FinalPoG_vec.at(1));  // MB estimator
			if (pog_px == BAD_POINT)
				pog_px = utils::round(FinalPoG_vec.at(0));  // IB estimator
		
		}
	}
	double endtTime_Estim  = (double)getTickCount();
	EstimTime = (endtTime_Estim - startTime_Estim)/tickFreq*1000;

	// ***********************************************************
	// 4. publish results to the shared memory
	if (g_pPublisher->IsUserCalibrationRequested())
	{
		if ( !g_pCalibrator->isCalibrating())
			g_pCalibrator->beginCalibration(false); //START CALIBRATION

		g_AutoCalibFlag = true;
		g_pPublisher->ResetUserCalibrationRequest();
	}		
	
	TIME_STAMP cur_image_time_stamp;
	cur_image_time_stamp.SYS_TIME   = ImTimeStamp.SYS_TIME;
	cur_image_time_stamp.TICK_COUNT = ImTimeStamp.TICK_COUNT;
	g_pPublisher->Publish(&cur_image_time_stamp, pSystemParams->GetDisplayPlanePtr(), g_pImageProcessor->getResultsPtr(), &Final_PoG_mm, &Raw_PoG_L_mm, &Raw_PoG_R_mm);

	// ***********************************************************
	// 5. save results to the output data struct	

	// system info
	ET_Data.screen_width_in_pixels  = pSystemParams->GetDisplayPlanePtr()->getWidth_px();
	ET_Data.screen_height_in_pixels = pSystemParams->GetDisplayPlanePtr()->getHeight_px();
	ET_Data.screen_width_in_mm      = pSystemParams->GetDisplayPlanePtr()->getWidth_mm();
	ET_Data.screen_height_in_mm     = pSystemParams->GetDisplayPlanePtr()->getHeight_px();

  	ET_Data.num_cameras        = 1;
	ET_Data.num_light_sources  = 2;
	ET_Data.num_eyes           = 2;

	// calibration status
	ET_Data.calibration_target_is_shown = g_pCalibrator->isCalibrating();

	// image processing results
	ET_Data.right_eye.camera[0].eye_found = (g_pImageProcessor->getResultsPtr()->isValid() & g_pImageProcessor->getResultsPtr()->getRightEyePtr()->isValid());
	if (ET_Data.right_eye.camera[0].eye_found)
	{
		ET_Data.right_eye.camera[0].pupil_x_pos_in_pixels   = g_pImageProcessor->getResultsPtr()->getRightEyePtr()->getPupilCenter().x;
		ET_Data.right_eye.camera[0].pupil_y_pos_in_pixels   = g_pImageProcessor->getResultsPtr()->getRightEyePtr()->getPupilCenter().y;
		ET_Data.right_eye.camera[0].pupil_area_in_mm_square = g_pImageProcessor->getResultsPtr()->getRightEyePtr()->getPupilBox().size.area();
	}

	ET_Data.left_eye.camera[0].eye_found = (g_pImageProcessor->getResultsPtr()->isValid() & g_pImageProcessor->getResultsPtr()->getLeftEyePtr()->isValid());
	if (ET_Data.left_eye.camera[0].eye_found)
	{
		ET_Data.left_eye.camera[0].pupil_x_pos_in_pixels   = g_pImageProcessor->getResultsPtr()->getLeftEyePtr()->getPupilCenter().x;
		ET_Data.left_eye.camera[0].pupil_y_pos_in_pixels   = g_pImageProcessor->getResultsPtr()->getLeftEyePtr()->getPupilCenter().y;
		ET_Data.left_eye.camera[0].pupil_area_in_mm_square = g_pImageProcessor->getResultsPtr()->getLeftEyePtr()->getPupilBox().size.area();
	}

	// PoG estimate
	if (pog_px == BAD_POINT || !isNewPoGAvailable)
	{
		ET_Data.right_eye_point_of_gaze_estimate.x_in_pixels = -1;
		ET_Data.right_eye_point_of_gaze_estimate.y_in_pixels = -1;
		ET_Data.right_eye_point_of_gaze_estimate.success     = false;

		ET_Data.left_eye_point_of_gaze_estimate.x_in_pixels = -1;
		ET_Data.left_eye_point_of_gaze_estimate.y_in_pixels = -1;
		ET_Data.left_eye_point_of_gaze_estimate.success     = false;
	}
	else
	{
		ET_Data.right_eye_point_of_gaze_estimate.x_in_pixels = pog_px.x;
		ET_Data.right_eye_point_of_gaze_estimate.y_in_pixels = pog_px.y;
		ET_Data.right_eye_point_of_gaze_estimate.success     = true;

		ET_Data.left_eye_point_of_gaze_estimate.x_in_pixels = pog_px.x;
		ET_Data.left_eye_point_of_gaze_estimate.y_in_pixels = pog_px.y;
		ET_Data.left_eye_point_of_gaze_estimate.success     = false;
	}

	// timing
	ET_Data.timestamp.year = ImTimeStamp.SYS_TIME.wYear;
	ET_Data.timestamp.month = ImTimeStamp.SYS_TIME.wMonth;
	ET_Data.timestamp.day = ImTimeStamp.SYS_TIME.wDay;
	ET_Data.timestamp.hour = ImTimeStamp.SYS_TIME.wHour;
	ET_Data.timestamp.minute = ImTimeStamp.SYS_TIME.wMinute;
	ET_Data.timestamp.second = ImTimeStamp.SYS_TIME.wSecond;
	ET_Data.timestamp.millisecond = ImTimeStamp.SYS_TIME.wMilliseconds;

}

void   HandleInputsCore(int inKey, ET_Settings& g_GlobalSettings, std::string PARAMS_FOLDER)
{
	if (inKey == VK_ESCAPE)
	{
		if (g_pCalibrator->isCalibrating())
		{
			g_pCalibrator->quitCalibration();
		}
	}
	else if (inKey == 'c' || inKey == 'C')
	{
		if ( !g_pCalibrator->isCalibrating())
			g_pCalibrator->beginCalibration(false); //START CALIBRATION
		g_AutoCalibFlag = true;
	}
	else if (inKey == 'k' || inKey == 'K')
	{
		if (!g_pCalibrator->isCalibrating())
			g_pCalibrator->beginCalibration(true); //START CALIBRATION
		g_AutoCalibFlag = false;
	}
	else if (inKey == VK_SPACE)
	{
		if (g_pCalibrator->isWaitingToContinue())
			g_pCalibrator->proceed(); // start recording for the next calibration point
	}
	else if (inKey == VK_BACK)
	{
		if (g_pCalibrator->isCalibrating())
		{
			g_pCalibrator->quitCalibration(); //End Calibration (without calibrating)
		}
	}
	else if (inKey == 'w' || inKey == 'W' )
	{
		g_pCalibrator->SaveUserParamsToFile(".");
	}
	else if (inKey == 'r' || inKey == 'R')
	{
		g_pCalibrator->LoadUserParamsFromFile(PARAMS_FOLDER, g_GlobalSettings.DEFAULT_USER_NAME);
	}
}

void   DeallocateCore()
{
	delete g_pCalibrator;
	delete g_pPogEstimatorIB;
	delete g_pPogEstimatorMB;
	delete g_pImageProcessor;
	delete g_pTrackDiagnostics;
	delete g_pPoG_FilterIB;
	delete g_pPoG_FilterMB;
	delete g_pPublisher;
}


#define NUM_FRAMES_TO_AVERAGE 10

double CalcFrameRate(double FrameTime)
{
	static double       FrameTimes[NUM_FRAMES_TO_AVERAGE];
	static unsigned int FrameCounter = 0;
	
	div_t cnt = div(FrameCounter, NUM_FRAMES_TO_AVERAGE);

	FrameTimes[cnt.rem] = FrameTime;
	FrameCounter++;

	int    AvailableFrames2Average = MIN(FrameCounter, NUM_FRAMES_TO_AVERAGE);
	double TotalTime               = 0;
	for (int n = 0; n < AvailableFrames2Average; n++)
		TotalTime += FrameTimes[n];

	double AvgFrameTime = TotalTime/AvailableFrames2Average;

	return 1000/AvgFrameTime;
}

