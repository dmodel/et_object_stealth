///////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//                           License Agreement
//
// This software is provided for evaluation purposes only. 
// Use in source and binary forms, with or without modification,
// is permitted with written consent of the copyright holder. 
//
// Redistribution of this software to third parties is strictly prohibited.
//
// This software is provided "as is" and without any express or implied warranties.
//
// By downloading, copying, installing or using the software you agree to this license.
// If you do not agree to this license, do not download, install,
// copy or use the software.
//
// To obtain a commercial license please contact the copyright holder.
//
// Copyright (C) 2013-2016, Eyediya Technologies Inc. (info@eyediya.ca)
// All rights reserved.
//
///////////////////////////////////////////////////////////////////////////////////////
#pragma once

#include <stdlib.h>
#include "opencv2\opencv.hpp"
#include "ET_Settings.h"
#include "SystemParams.h"
#include "eye_tracker_data.h"

#ifdef EYETRACKERCORE_EXPORTS
#define ETCORE_API __declspec(dllexport) 
#else
#define ETCORE_API __declspec(dllimport) 
#endif

struct IMAGE_TIME_STAMP
{
	double       TICK_COUNT;
	SYSTEMTIME   SYS_TIME;
};


// interface functions
void   ETCORE_API ProcessNextImageCore(cv::Mat_<uchar> processImage, cv::Mat_<uchar> displayImage, IMAGE_TIME_STAMP& ImTimeStamp, SystemParams* pSystemParams, Eye_tracker_data& ET_Data);
bool   ETCORE_API InitCore(ET_Settings& g_GlobalSettings, SystemParams* pSystemParams, std::string PARAMS_FOLDER);
void   ETCORE_API DeallocateCore();
void   ETCORE_API HandleInputsCore(int inKey, ET_Settings& g_GlobalSettings, std::string PARAMS_FOLDER);
