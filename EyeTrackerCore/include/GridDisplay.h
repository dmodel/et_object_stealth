///////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//                           License Agreement
//
// This software is provided for evaluation purposes only. 
// Use in source and binary forms, with or without modification,
// is permitted with written consent of the copyright holder. 
//
// Redistribution of this software to third parties is strictly prohibited.
//
// This software is provided "as is" and without any express or implied warranties.
//
// By downloading, copying, installing or using the software you agree to this license.
// If you do not agree to this license, do not download, install,
// copy or use the software.
//
// To obtain a commercial license please contact the copyright holder.
//
// Copyright (C) 2013-2016, Eyediya Technologies Inc. (info@eyediya.ca)
// All rights reserved.
//
///////////////////////////////////////////////////////////////////////////////////////
#pragma once

#include "opencv2\opencv.hpp"
#include "SystemParams.h"

#ifdef _WINDOWS
#include "graphicwindow.h"
#include "displayPlane.h"
#define CV_RGBA(r, g, b)     Scalar(b, g, r)
#else
#define CV_RGBA(r, g, b)     Scalar(r, g, b, 255)
#endif

#define CV_COLOR_RED         CV_RGBA(200,0,0)
#define CV_COLOR_GREEN       CV_RGBA(0,200,0)
#define CV_COLOR_BLUE        CV_RGBA(0,0,200)
#define CV_COLOR_LIGHTBLUE   CV_RGBA(100,100,255)
#define CV_COLOR_WHITE       CV_RGBA(255,255,255)
#define CV_COLOR_BLACK       CV_RGBA(0,0,0)
#define CV_COLOR_GREY        CV_RGBA(200,200,200)
#define CV_COLOR_YELLOW      CV_RGBA(255,255,0)
#define CV_COLOR_MAGENTA     CV_RGBA(255,0,255)

#ifdef  GRAPHICWINDOW_EXPORTS
#define GRAPHICWINDOW_API __declspec(dllexport) 
#else
#define GRAPHICWINDOW_API __declspec(dllimport) 
#endif


#ifdef _WINDOWS
class GRAPHICWINDOW_API GridDisplay:public GraphicWindow
#else
class GridDisplay
#endif

{
public:
    GridDisplay(int N_cols, int M_rows, DisplayPlane* pDisplayPlane, cv::Mat* pWatermark = NULL);
    ~GridDisplay();

    void updateFaceImage(cv::Mat* pImage); // updates image in the corner
    void updateXY(int x, int y); // updates cursor location and highlighted rectangle
    void updateXY(std::vector<cv::Point> CurPosVec); // updates cursor location and highlighted rectangle
    cv::Mat* GetImageToShow(); // returns pointer to Mat holding displayImage
    void HideCursor(); // will hide pointer (remove from image and not show until ShowCursor() is called)
    void ShowCursor(); // will show pointer on the image
	void EnableHighlight();
	void DisableHighlight();
	bool IsTapInBottomRightSquare(int x, int y);
	bool IsTapInBottomLeftSquare(int x, int y);
	bool IsTapInTopLeftSquare(int x, int y);
	bool IsTapInTopRightSquare(int x, int y);
	bool IsTapInSquareMN(int x, int y, int Row, int Col);
	cv::Rect GetInsetRoiRect();
	void AddTextMN(int Row, int Col, const char text[]);
	void SetColorMN(int Row, int Col, cv::Scalar colour);
	void displayImage();
    cv::Rect GetGridROI(int grid_c, int grid_r);
    cv::Rect GetGridROI(cv::Point pt);
	void GetColumnRow(cv::Point pt, int& col, int& row);

private:
    bool m_show_cursor;
    bool m_update_highlight;
    bool m_valid_flag;
    int m_width_px;
    int m_height_px;
    int m_number_cols;
    int m_number_rows;
    int m_grid_width;
    int m_grid_height;
    int m_left_board_thickness;
    int m_top_board_thickness;
    int m_hightlight_col;
    int m_hightlight_row;

	std::vector<cv::Point>  m_CurCursorPosVec;
    std::vector<cv::Point>  m_PrevCursorPosVec;
	std::vector<cv::Scalar> m_CursorColorVec;

    cv::Mat  m_display_image;
    cv::Mat  m_display_backup_image;
    cv::Mat  m_original_image;
    cv::Mat  m_highlight_image;
    cv::Mat  m_text_mask;
    cv::Mat  m_cursor_alpha;
    cv::Rect m_InsetRoiRect;
    cv::Mat  m_WatermarkImage;

    void InitializeDisplayImage();
    void InitializeWatermark();
    void InitializeCursorImage();
    void CreateWatermarkImage(cv::Mat *pWatermark);

    void RendToGrid(int grid_c, int grid_r, cv::Mat *p_image); // rend to the center of grid

    void UndrawCursor();
    void DrawCursor();
    void UpdateHighlight(cv::Point CurPos);

    // Utilities
    float CalculateDisplayResizeRatio(int image_width, int image_height, int display_width, int display_height);

	DisplayPlane* m_pDisplayPlane;
};