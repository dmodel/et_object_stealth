///////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//                           License Agreement
//
// This software is provided for evaluation purposes only. 
// Use in source and binary forms, with or without modification,
// is permitted with written consent of the copyright holder. 
//
// Redistribution of this software to third parties is strictly prohibited.
//
// This software is provided "as is" and without any express or implied warranties.
//
// By downloading, copying, installing or using the software you agree to this license.
// If you do not agree to this license, do not download, install,
// copy or use the software.
//
// To obtain a commercial license please contact the copyright holder.
//
// Copyright (C) 2013-2016, Eyediya Technologies Inc. (info@eyediya.ca)
// All rights reserved.
//
///////////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "StdAfx.h"
#include "imageprocessor.h"
#include "GazeEstimator.h"
#include "DisplayPlane.h"
#include "opencv2\opencv.hpp"

#ifdef  PUBLISHER_EXPORTS
#define PUBLISHER_API __declspec(dllexport) 
#else
#define PUBLISHER_API __declspec(dllimport) 
#endif

struct TIME_STAMP
{
	double       TICK_COUNT;
	SYSTEMTIME   SYS_TIME;
};

class PUBLISHER_API Publisher {
public:
	Publisher(void);
	~Publisher(void);

	void		 Publish(TIME_STAMP* pTimeStamp, DisplayPlane* pDisplayPlane, ImageProcessingResult*	pImProcResult, GazeEstimation* Final_PoG, GazeEstimation* Raw_PoG_L = NULL, GazeEstimation* Raw_PoG_R = NULL);
	
	bool		 IsUserCalibrationRequested();
	void		 ResetUserCalibrationRequest();
	unsigned int GetFrameCount() {return m_FrameCounter;};

private:
	HANDLE  m_HFileMapping; 		// identifier for File-mapping object
	double* m_pPtrSharedMemory; 	// pointer to shared memory
	double* m_pPtrFileMemory;	    
	HANDLE  m_HMappingForFile; 		

	void CopySharedMem2File();	    

	unsigned int m_FrameCounter;
};


