///////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//                           License Agreement
//
// This software is provided for evaluation purposes only. 
// Use in source and binary forms, with or without modification,
// is permitted with written consent of the copyright holder. 
//
// Redistribution of this software to third parties is strictly prohibited.
//
// This software is provided "as is" and without any express or implied warranties.
//
// By downloading, copying, installing or using the software you agree to this license.
// If you do not agree to this license, do not download, install,
// copy or use the software.
//
// To obtain a commercial license please contact the copyright holder.
//
// Copyright (C) 2013-2016, Eyediya Technologies Inc. (info@eyediya.ca)
// All rights reserved.
//
///////////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "EyeFeatures.h"

#ifdef _WINDOWS

#ifdef IMAGEPROCESSOR_EXPORTS
#define IMAGEPROCESSOR_API __declspec(dllexport) 
#else
#define IMAGEPROCESSOR_API __declspec(dllimport) 
#endif

class IMAGEPROCESSOR_API ImageProcessingResult

#else

class ImageProcessingResult

#endif

{
public:
	ImageProcessingResult(); 
	ImageProcessingResult(EyeFeatures* pLeftEye, EyeFeatures* pRightEye, int HorFlipW = 0);
	ImageProcessingResult(ImageProcessingResult* pImageProcessingResult, int HorFlipW = 0);

	EyeFeatures  getLeftEye();
	EyeFeatures  getRightEye();

	EyeFeatures* getLeftEyePtr()  {return &m_leftEye;};
	EyeFeatures* getRightEyePtr() {return &m_rightEye;};

	bool         isValid()                {return m_bValid;};
	void         setValidity(bool bValid) {m_bValid = bValid;};

	void         ScaleCrIntensities(float Scale, float MaxVal = 255);

private:
	EyeFeatures  m_leftEye;
	EyeFeatures  m_rightEye;
	bool         m_bValid;
};

