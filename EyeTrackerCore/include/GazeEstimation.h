///////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//                           License Agreement
//
// This software is provided for evaluation purposes only. 
// Use in source and binary forms, with or without modification,
// is permitted with written consent of the copyright holder. 
//
// Redistribution of this software to third parties is strictly prohibited.
//
// This software is provided "as is" and without any express or implied warranties.
//
// By downloading, copying, installing or using the software you agree to this license.
// If you do not agree to this license, do not download, install,
// copy or use the software.
//
// To obtain a commercial license please contact the copyright holder.
//
// Copyright (C) 2013-2016, Eyediya Technologies Inc. (info@eyediya.ca)
// All rights reserved.
//
///////////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "opencv2\opencv.hpp"

#include "MathDataTypes.h"

#define BAD_POINT_COORD     -1000
#define BAD_POINT			cv::Point(BAD_POINT_COORD, BAD_POINT_COORD)
#define BAD_POINT_2F		cv::Point2f(BAD_POINT_COORD, BAD_POINT_COORD)
#define BAD_POINT_2D		Point2D(BAD_POINT_COORD, BAD_POINT_COORD)
#define BAD_POINT_3D		Point3D(BAD_POINT_COORD, BAD_POINT_COORD, BAD_POINT_COORD)

#ifdef _WINDOWS

#ifdef  GAZEESTIMATOR_EXPORTS
#define GAZEESTIMATOR_API __declspec(dllexport) 
#else
#define GAZEESTIMATOR_API __declspec(dllimport) 
#endif

class GAZEESTIMATOR_API GazeEstimation

#else

class GazeEstimation

#endif

{
public:
	GazeEstimation();
	GazeEstimation(Point3D& pog);	
	~GazeEstimation();

	// Set member functions
	void setValid(bool valid) {m_bIsValid = valid;};

	void setPoG(Point3D& pog);
	void setD(Point3D& d) {m_d = d;};
	void setD(Vec3D& d) {m_d = Point3D(d);};
	void setC(Point3D& c) {m_c = c;};
	void setC(Vec3D& c) {m_c = Point3D(c);};
	void setP(Point3D& p) {m_p = p;};
	void setP(Vec3D& p) {m_p = Point3D(p);};
	void setOptAxis(Point3D& optic_axis) {m_optic_axis = optic_axis;};
	void setOptAxis(Vec3D& optic_axis) {m_optic_axis = Point3D(optic_axis);};
	void setVisAxis(Point3D& visual_axis) {m_visual_axis = visual_axis;};
	void setVisAxis(Vec3D& visual_axis) {m_visual_axis = Point3D(visual_axis);};
	void setThetaEyeDeg(Real theta_eye_deg) {m_theta_eye_deg = theta_eye_deg;};
	void setPhiEyeDeg(Real phi_eye_deg) {m_phi_eye_deg = phi_eye_deg;};
	void setPoG_raw(Point3D& pog_raw) {m_pog_raw = pog_raw;};
	void setPoG_raw(Vec3D& pog_raw) {m_pog_raw = Point3D(pog_raw);};

	// Get / check member functions
	bool isValid() {return m_bIsValid;};
	
	Point3D getPoG() {return m_pog;};
	Point3D getD() {return m_d;}; // 3D coordinates of the center of rotation of the eye w.r.t. the WCS.
	Point3D getC() {return m_c;}; // 3D coordinates of the center of curvature of the cornea w.r.t. the WCS.
	Point3D getP() {return m_p;}; // 3D coordinates of the pupil center w.r.t. the WCS.
	Point3D getOptAxis() {return m_optic_axis;}; // Unit vector in the direction of the optic axis w.r.t. the WCS.
	Point3D getVisAxis() {return m_visual_axis;}; // Unit vector in the direction of the visual axis w.r.t. the WCS.
	Real    getThetaEyeDeg() {return m_theta_eye_deg;}; // Pan angle of the optic axis of the eye in degrees w.r.t. the WCS.
	Real    getPhiEyeDeg()   {return m_phi_eye_deg;};   // Tilt angle of the optic axis of the eye in degrees w.r.t. the WCS.
	Point3D getPoG_raw() {return m_pog_raw;};           // 3D coordinates of the point-of-gaze w.r.t. the WCS in mm before homography.


private:
	Point3D m_pog; // Final point-of-gaze estimate after any post-processing (e.g., homography) if applicable, w.r.t. the WCS in mm.
	Point3D m_d; // 3D coordinates of the center of rotation of the eye w.r.t. the WCS.
	Point3D m_c; // 3D coordinates of the center of curvature of the cornea w.r.t. the WCS.
	Point3D m_p; // 3D coordinates of the pupil center w.r.t. the WCS.
	Point3D m_optic_axis; // Unit vector in the direction of the optic axis w.r.t. the WCS.
	Point3D m_visual_axis; // Unit vector in the direction of the visual axis w.r.t. the WCS.
	Real    m_theta_eye_deg; // Pan angle of the optic axis of the eye in degrees w.r.t. the WCS.
	Real    m_phi_eye_deg; // Tilt angle of the optic axis of the eye in degrees w.r.t. the WCS.
	Point3D m_pog_raw; // 3D coordinates of the point-of-gaze w.r.t. the WCS in mm before homography
	bool    m_bIsValid;
};
