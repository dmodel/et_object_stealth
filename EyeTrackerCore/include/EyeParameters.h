///////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//                           License Agreement
//
// This software is provided for evaluation purposes only. 
// Use in source and binary forms, with or without modification,
// is permitted with written consent of the copyright holder. 
//
// Redistribution of this software to third parties is strictly prohibited.
//
// This software is provided "as is" and without any express or implied warranties.
//
// By downloading, copying, installing or using the software you agree to this license.
// If you do not agree to this license, do not download, install,
// copy or use the software.
//
// To obtain a commercial license please contact the copyright holder.
//
// Copyright (C) 2013-2016, Eyediya Technologies Inc. (info@eyediya.ca)
// All rights reserved.
//
///////////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "opencv2\opencv.hpp"

#include "MathDataTypes.h"

#ifdef _WINDOWS
#ifdef  GAZEESTIMATOR_EXPORTS
#define GAZEESTIMATOR_API __declspec(dllexport) 
#else
#define GAZEESTIMATOR_API __declspec(dllimport) 
#endif

class GAZEESTIMATOR_API EyeParameters

#else

class EyeParameters


#endif
{
public:
	EyeParameters();
	EyeParameters(EyeParameters* pEyeParameters);
	EyeParameters(Real r, Real k, Real alpha, Real beta, const Matx3x3& H);
	EyeParameters(std::vector<Point2D> gain,  std::vector<Point2D> off, std::vector<cv::Mat> CalibMat, Point3D AvgEyePos);
	~EyeParameters();

	Real     getR()     {return m_r;};
	Real     getK()     {return m_k;};
	Real     getAlpha() {return m_alpha;};
	Real     getBeta()  {return m_beta;};
	Matx3x3& getH()     {return m_H;};
	
	std::vector<Point2D>  getGain()     {return m_gain;};
	std::vector<Point2D>  getOffset()   {return m_off;};
	std::vector<cv::Mat>  getCalibMat() {return m_CalibMat;};

	std::vector<Point2D>* getGainPtr()   {return &m_gain;};
	std::vector<Point2D>* getOffsetPtr() {return &m_off;};
	std::vector<cv::Mat>* getCalibMatPtr() {return &m_CalibMat;};

	bool    isValid_IB() {return m_isValid_IB;};
	bool    isValid_MB() {return m_isValid_MB;};

	void    setR(Real R)				{m_r     = R;};
	void    setK(Real K)				{m_k     = K;};
	void    setAlpha(Real Alpha)		{m_alpha = Alpha;};
	void    setBeta(Real Beta)			{m_beta  = Beta;};
	void	setHomography(Matx3x3& H)	{m_H	 = H;};
	void	setHomography2Identity()	{m_H     = Matx3x3::eye();};

	void    setGain(std::vector<Point2D>   gain)  {m_gain     = gain;};
	void    setOffset(std::vector<Point2D> off)   {m_off      = off;};
	void    setCalibMat(std::vector<cv::Mat>   mat)   {m_CalibMat = mat;};

	void    setValid_IB(bool valid = true)   {m_isValid_IB = valid;}; 
	void    setValid_MB(bool valid = true)   {m_isValid_MB = valid;}; 

	void	SaveUserParamsToFile(std::string   fileName);
	bool	LoadUserParamsFromFile(std::string fileName);

	Point3D GetAvgEyePos()					{return m_AvgEyePos;};
	void    SetAvgEyePos(Point3D avgEyePos) {m_AvgEyePos = avgEyePos;};

private:
	Real			m_r;
	Real			m_k;
	Real			m_alpha;
	Real			m_beta;
	Matx3x3			m_H;

	bool    m_isValid_IB;
	bool    m_isValid_MB;

	std::vector<Point2D> m_gain;
	std::vector<Point2D> m_off;
	std::vector<cv::Mat> m_CalibMat;

	Point3D			m_AvgEyePos;
};

