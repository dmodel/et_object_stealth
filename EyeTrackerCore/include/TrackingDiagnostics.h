///////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//                           License Agreement
//
// This software is provided for evaluation purposes only. 
// Use in source and binary forms, with or without modification,
// is permitted with written consent of the copyright holder. 
//
// Redistribution of this software to third parties is strictly prohibited.
//
// This software is provided "as is" and without any express or implied warranties.
//
// By downloading, copying, installing or using the software you agree to this license.
// If you do not agree to this license, do not download, install,
// copy or use the software.
//
// To obtain a commercial license please contact the copyright holder.
//
// Copyright (C) 2013-2016, Eyediya Technologies Inc. (info@eyediya.ca)
// All rights reserved.
//
///////////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "opencv2\opencv.hpp"
#include "ImageProcessingResult.h"


#ifdef _WINDOWS
#ifdef  IMAGEPROCESSOR_EXPORTS
#define IMAGEPROCESSOR_API __declspec(dllexport) 
#else
#define IMAGEPROCESSOR_API __declspec(dllimport) 
#endif
#endif

enum SweepMode
{
	HorQuaters = 0,
	VerQuaters = 1
};

enum OpMode
{
	Tracking = 0,
	Sweeping = 1
};

#ifdef _WINDOWS
struct IMAGEPROCESSOR_API IlluminationControl
#else
struct IlluminationControl
#endif
{
	IlluminationControl()
	{
		MinLevel.clear();
		MaxLevel.clear();
		CurLevel.clear();
		NextLevel.clear();
		SweepStepSize.clear();
	}

	IlluminationControl(std::vector<float> minLvl, std::vector<float> maxLvl, std::vector<float> curLvl, std::vector<float> StepSz)
	{
		MinLevel      = minLvl;
		MaxLevel      = maxLvl;
		CurLevel      = curLvl;
		NextLevel     = curLvl;
		SweepStepSize = StepSz;
	}

	std::vector<float> MinLevel;
	std::vector<float> MaxLevel;
	std::vector<float> CurLevel;
	std::vector<float> NextLevel;
	std::vector<float> SweepStepSize;
};


#ifdef _WINDOWS
class IMAGEPROCESSOR_API TrackingDiagnostics
#else
class TrackingDiagnostics
#endif
{
public:
	TrackingDiagnostics(cv::Size ImageSize, unsigned int FrameRate = 10, IlluminationControl LEDsCtrl = IlluminationControl(), SweepMode CurSweepMode = HorQuaters);
	~TrackingDiagnostics();

	void   UpdateResults(ImageProcessingResult* pCurrentResult);
	void   SetSweepMode(SweepMode CurSweepMode);
	OpMode GetOpMode() {return m_OpMode;};

	std::vector<float> GetNextIlluminationLevel()              {return m_LEDsCtrl.NextLevel;};
	void  SetCurIlluminationLevel(std::vector<float> curLevel) {m_LEDsCtrl.CurLevel = curLevel;};

	std::vector<cv::Rect>  GetNextROIsVec();

private:
	void SetSweepROIs(SweepMode CurSweepMode);
	void DetermineNextIlluminationLevel();
	void ResetLEDsLevelToAvg();

	ImageProcessingResult m_lastGoodResultsForBothEyes;
	ImageProcessingResult m_lastConfirmedResultForOneEyes;
	ImageProcessingResult m_lastGoodResult;
	ImageProcessingResult m_lastResult;

	cv::Size			  m_ImageSize;

	IlluminationControl   m_LEDsCtrl;

	unsigned int          m_FrameRate;

	unsigned int          m_Counter_FramesSinceOneEyeLost;
	unsigned int          m_Counter_FramesSinceBothEyesLost;
	unsigned int          m_Counter_FramesUnconfirmed;
	std::vector<cv::Rect> m_RoiSweepVector;

	OpMode                m_OpMode;
	SweepMode             m_SweepMode;
};
