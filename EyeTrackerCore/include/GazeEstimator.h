///////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//                           License Agreement
//
// This software is provided for evaluation purposes only. 
// Use in source and binary forms, with or without modification,
// is permitted with written consent of the copyright holder. 
//
// Redistribution of this software to third parties is strictly prohibited.
//
// This software is provided "as is" and without any express or implied warranties.
//
// By downloading, copying, installing or using the software you agree to this license.
// If you do not agree to this license, do not download, install,
// copy or use the software.
//
// To obtain a commercial license please contact the copyright holder.
//
// Copyright (C) 2013-2016, Eyediya Technologies Inc. (info@eyediya.ca)
// All rights reserved.
//
///////////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "SystemParams.h"
#include "ImageProcessor.h"
#include "UserParameters.h"
#include "GazeEstimation.h"
#include "HeadPose.h"

enum GazeEstimtorType
{
	PG1CAM = 0,
	INTSCL = 1
};

#ifdef _WINDOWS
#ifdef  GAZEESTIMATOR_EXPORTS
#define GAZEESTIMATOR_API __declspec(dllexport) 
#else
#define GAZEESTIMATOR_API __declspec(dllimport) 
#endif
#endif


class GazeEstBaseClass
{
public:
	virtual GazeEstimation calcPogBothEyes(ImageProcessingResult* pImProcResult, UserParameters* pUserParameters, GazeEstimation& RawGazeEst_L_Out, GazeEstimation& RawGazeEst_R_Out, Real RotAngle, SystemParams* pSysParams = NULL) = 0; 
	virtual GazeEstimation calcPogSingleEye(EyeFeatures* pEyeFeatures, HeadPose* pHeadPose, EyeParameters* pEyeParams, Real RotAngle, SystemParams* pSysParams = NULL) = 0;
};

#ifdef _WINDOWS
class GAZEESTIMATOR_API GazeEstimator
#else
class GazeEstimator
#endif
{
public:
	GazeEstimator(GazeEstimtorType EstType);
	~GazeEstimator();

	GazeEstimation calcPogBothEyes(ImageProcessingResult* pImProcResult, UserParameters* pUserParameters, GazeEstimation& RawGazeEst_L_Out, GazeEstimation& RawGazeEst_R_Out, Real RotAngle = 0, SystemParams* pSysParams = NULL); 
	GazeEstimation calcPogSingleEye(EyeFeatures* pEyeFeatures, HeadPose* pHeadPose, EyeParameters* pEyeParams, Real RotAngle = 0, SystemParams* pSysParams = NULL);

	GazeEstimtorType GetEstimatorType() {return m_EstimatorType;};

protected:
	GazeEstimtorType  m_EstimatorType;
	GazeEstBaseClass* m_pGazeEstimator;   
};

