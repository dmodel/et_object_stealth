///////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//                           License Agreement
//
// This software is provided for evaluation purposes only. 
// Use in source and binary forms, with or without modification,
// is permitted with written consent of the copyright holder. 
//
// Redistribution of this software to third parties is strictly prohibited.
//
// This software is provided "as is" and without any express or implied warranties.
//
// By downloading, copying, installing or using the software you agree to this license.
// If you do not agree to this license, do not download, install,
// copy or use the software.
//
// To obtain a commercial license please contact the copyright holder.
//
// Copyright (C) 2013-2016, Eyediya Technologies Inc. (info@eyediya.ca)
// All rights reserved.
//
///////////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "CalibrationRecord.h"

#ifdef _WINDOWS

#ifdef  CALIBRATOR_EXPORTS
#define CALIBRATOR_API __declspec(dllexport) 
#else
#define CALIBRATOR_API __declspec(dllimport) 
#endif

class CALIBRATOR_API CalibrationDataPoint

#else

class CalibrationDataPoint

#endif

{
public:
	CalibrationDataPoint(Point3D* CalibTarget);
	~CalibrationDataPoint();

	void               addRecord(CalibrationRecord* record);
	CalibrationRecord* getRecordPtr(unsigned int index);
	void               removeRecord(unsigned int index);
	Point3D            getCalibTarget()	   {return m_CalibTarget;};
	Point3D*           getCalibTargetPtr() {return &m_CalibTarget;};
	unsigned int       getNumRecords();


private:
	std::vector<CalibrationRecord*> m_records;
	Point3D							m_CalibTarget;
};

