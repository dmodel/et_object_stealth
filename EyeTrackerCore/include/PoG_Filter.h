///////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//                           License Agreement
//
// This software is provided for evaluation purposes only. 
// Use in source and binary forms, with or without modification,
// is permitted with written consent of the copyright holder. 
//
// Redistribution of this software to third parties is strictly prohibited.
//
// This software is provided "as is" and without any express or implied warranties.
//
// By downloading, copying, installing or using the software you agree to this license.
// If you do not agree to this license, do not download, install,
// copy or use the software.
//
// To obtain a commercial license please contact the copyright holder.
//
// Copyright (C) 2013-2016, Eyediya Technologies Inc. (info@eyediya.ca)
// All rights reserved.
//
///////////////////////////////////////////////////////////////////////////////////////
#pragma once
#include "opencv2\opencv.hpp"
#include "GazeEstimation.h"

#ifdef _WINDOWS
#ifdef  GAZEESTIMATOR_EXPORTS
#define GAZEESTIMATOR_API __declspec(dllexport) 
#else
#define GAZEESTIMATOR_API __declspec(dllimport) 
#endif
#endif

struct PoG_Cluster
{
	PoG_Cluster()
	{
		PoG      = BAD_POINT_3D;
		IsLocked = false;
	}
			
	PoG_Cluster(Point3D PoG_in, bool isLocked = false)
	{
		PoG      = PoG_in;
		IsLocked = isLocked;
	}

	Point3D PoG;
	bool    IsLocked;
};

enum FilterType
{
	AVG = 0,
	DIR = 1,
	HBD = 2
};

class PoGFilterBase
{
public:
	virtual GazeEstimation UpdatePoG(GazeEstimation* pPoG_L, GazeEstimation* pPoG_R, bool& PoG_Updated) = 0;
	virtual bool           IsClusterLocked() = 0;
	virtual bool           IsPogInCluster()  = 0;
	virtual PoG_Cluster    GetPoGCluster()   = 0;

};


#ifdef _WINDOWS
class GAZEESTIMATOR_API PoG_Filter
#else
class PoG_Filter
#endif
{
public:
	PoG_Filter(FilterType FiltType);
	~PoG_Filter();

	GazeEstimation UpdatePoG(GazeEstimation* pPoG_L, GazeEstimation* pPoG_R, bool& PoG_Updated);
	bool           IsClusterLocked();
	bool           IsPogInCluster();
	PoG_Cluster    GetPoGCluster();
	FilterType	   GetFilterType();

private:
	PoGFilterBase* m_pFilter;
	FilterType     m_FilterType;
};

