///////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//                           License Agreement
//
// This software is provided for evaluation purposes only. 
// Use in source and binary forms, with or without modification,
// is permitted with written consent of the copyright holder. 
//
// Redistribution of this software to third parties is strictly prohibited.
//
// This software is provided "as is" and without any express or implied warranties.
//
// By downloading, copying, installing or using the software you agree to this license.
// If you do not agree to this license, do not download, install,
// copy or use the software.
//
// To obtain a commercial license please contact the copyright holder.
//
// Copyright (C) 2013-2016, Eyediya Technologies Inc. (info@eyediya.ca)
// All rights reserved.
//
///////////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "opencv2\opencv.hpp"

#include "MathDataTypes.h"

#ifdef _WINDOWS
#include <windows.h>

#ifdef  SYSTEMPARAMS_EXPORTS
#define SYSTEMPARAMS_API __declspec(dllexport) 
#else
#define SYSTEMPARAMS_API __declspec(dllimport) 
#endif

class SYSTEMPARAMS_API DisplayPlane
#else
#include <jni.h>
class DisplayPlane
#endif
{   
public:
	DisplayPlane(Real width_mm, Real height_mm, unsigned int width_px = 0, unsigned int height_px = 0, unsigned int  monitor_number = 1);

	Real getWidth_mm();
	Real getHeight_mm();

	unsigned int getWidth_px();
	unsigned int getHeight_px();

	unsigned int get_monitor_offset_x_px();
	unsigned int get_monitor_offset_y_px();
	unsigned int get_monitor_number();

	Point3D DCS2WCS(Point3D pt_ICS_px); // 3D point in the DCS
	Point3D DCS2WCS(Point2D pt_ICS_px); // 2D point in the DCS (the third coordinate is assumed to be 0)

	Point3D WCS2DCS_3D(Point3D pt_WCS_mm); // 3D point in the DCS
	Point2D WCS2DCS(Point3D pt_WCS_mm);    // 2D point in the DCS (the third coordinate is assumed to be 0)

	bool FindLineDisplayIntersection_WCS(Point3D x0, Point3D dir, Point3D& result);

private:

	// X - Axis, Y - axis and Origin of the display plane in the World Coordinate System
	Point3D m_X_axis; 
    Point3D m_Y_axis; 
	Point3D m_Origin_mm; 
	Point3D m_PixelPitch_mm_per_px;

	//alternative representation of plane equation: normal*x + offset = 0
    Point3D m_Normal; 
    Real    m_Offset; 

	// physical parameters
	Real    m_width_mm; 
	Real    m_height_mm;

	// screen parameters
	unsigned int  m_width_px; 
	unsigned int  m_height_px;
	unsigned int  m_monitor_number; // for multiple display - select appropriate monitor
	int           m_monitor_offset_x_px; // logical X offset of the origin of the monitor in pixels
	int           m_monitor_offset_y_px; // logical Y offset of the origin of the monitor in pixels
};