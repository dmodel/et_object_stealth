///////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//                           License Agreement
//
// This software is provided for evaluation purposes only. 
// Use in source and binary forms, with or without modification,
// is permitted with written consent of the copyright holder. 
//
// Redistribution of this software to third parties is strictly prohibited.
//
// This software is provided "as is" and without any express or implied warranties.
//
// By downloading, copying, installing or using the software you agree to this license.
// If you do not agree to this license, do not download, install,
// copy or use the software.
//
// To obtain a commercial license please contact the copyright holder.
//
// Copyright (C) 2013-2016, Eyediya Technologies Inc. (info@eyediya.ca)
// All rights reserved.
//
///////////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "displayPlane.h"
#include "Camera.h"
#include "opencv2\opencv.hpp"

#ifdef _WINDOWS

#ifdef  SYSTEMPARAMS_EXPORTS
#define SYSTEMPARAMS_API __declspec(dllexport) 
#else
#define SYSTEMPARAMS_API __declspec(dllimport) 
#endif

class SYSTEMPARAMS_API SystemParams

#else

class SystemParams

#endif
{
public:
	SystemParams(std::string ParamsFileName, unsigned int width_px = 0, unsigned int height_px = 0, unsigned int N_display = 1);

	std::vector<Point3D> GetLSVec();
	Point3D				 GetLS(unsigned int n);

	Camera*				 GetCameraPtr(unsigned int n = 0);
	DisplayPlane*		 GetDisplayPlanePtr(unsigned int n = 0);

private:
	std::vector<Point3D> LS_vec;

	std::vector<Camera> CAM_vec;

	std::vector<DisplayPlane> DISP_vec;
};
