///////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//                           License Agreement
//
// This software is provided for evaluation purposes only. 
// Use in source and binary forms, with or without modification,
// is permitted with written consent of the copyright holder. 
//
// Redistribution of this software to third parties is strictly prohibited.
//
// This software is provided "as is" and without any express or implied warranties.
//
// By downloading, copying, installing or using the software you agree to this license.
// If you do not agree to this license, do not download, install,
// copy or use the software.
//
// To obtain a commercial license please contact the copyright holder.
//
// Copyright (C) 2013-2016, Eyediya Technologies Inc. (info@eyediya.ca)
// All rights reserved.
//
///////////////////////////////////////////////////////////////////////////////////////

#pragma once
#include <string>
#include "EyeParameters.h"

#ifdef _WINDOWS

#ifdef  GAZEESTIMATOR_EXPORTS
#define GAZEESTIMATOR_API __declspec(dllexport) 
#else
#define GAZEESTIMATOR_API __declspec(dllimport) 
#endif

class GAZEESTIMATOR_API UserParameters

#else

class UserParameters

#endif

{
public:
	UserParameters();
	UserParameters(EyeParameters* pRightEye, EyeParameters* pLeftEye);
	UserParameters(const char* pUserName, EyeParameters* pRightEye, EyeParameters* pLeftEye);
	~UserParameters();

	EyeParameters  getRightEyeParams();
	EyeParameters  getLeftEyeParams();

	EyeParameters* getRightEyeParamsPtr() {return &m_rightEye;};
	EyeParameters* getLeftEyeParamsPtr()  {return &m_leftEye;};

	std::string    getUserName();

	void SaveUserParamsToFile(std::string FolderName);  

	bool LoadUserParamsFromFile(std::string FolderName, std::string UserName); 

private:
	std::string        m_userName;
	EyeParameters      m_rightEye;
	EyeParameters      m_leftEye;

};

