///////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//                           License Agreement
//
// This software is provided for evaluation purposes only. 
// Use in source and binary forms, with or without modification,
// is permitted with written consent of the copyright holder. 
//
// Redistribution of this software to third parties is strictly prohibited.
//
// This software is provided "as is" and without any express or implied warranties.
//
// By downloading, copying, installing or using the software you agree to this license.
// If you do not agree to this license, do not download, install,
// copy or use the software.
//
// To obtain a commercial license please contact the copyright holder.
//
// Copyright (C) 2013-2016, Eyediya Technologies Inc. (info@eyediya.ca)
// All rights reserved.
//
///////////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "StdAfx.h"
#include "MathDataTypes.h"

#ifdef _WINDOWS
#ifdef  GAZEESTIMATOR_EXPORTS
#define GAZEESTIMATOR_API __declspec(dllexport) 
#else
#define GAZEESTIMATOR_API __declspec(dllimport) 
#endif

class GAZEESTIMATOR_API HeadPose
#else
class HeadPose
#endif

{
public:
	HeadPose();
	HeadPose(Real pitch_rad, Real yaw_rad, Real roll_rad);
	HeadPose(HeadPose* headPose);
	Real	getPitchRad();
	Real	getYawRad();
	Real	getRollRad();
	bool    isValid() {return m_bValid;};

private:
	Real	m_pitch_rad;
	Real	m_yaw_rad;
	Real	m_roll_rad;
	bool    m_bValid;
};

