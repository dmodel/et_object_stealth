///////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//                           License Agreement
//
// This software is provided for evaluation purposes only. 
// Use in source and binary forms, with or without modification,
// is permitted with written consent of the copyright holder. 
//
// Redistribution of this software to third parties is strictly prohibited.
//
// This software is provided "as is" and without any express or implied warranties.
//
// By downloading, copying, installing or using the software you agree to this license.
// If you do not agree to this license, do not download, install,
// copy or use the software.
//
// To obtain a commercial license please contact the copyright holder.
//
// Copyright (C) 2013-2016, Eyediya Technologies Inc. (info@eyediya.ca)
// All rights reserved.
//
///////////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "opencv2\opencv.hpp"


#ifdef _WINDOWS

#ifdef IMAGEPROCESSOR_EXPORTS
#define IMAGEPROCESSOR_API __declspec(dllexport) 
#else
#define IMAGEPROCESSOR_API __declspec(dllimport) 
#endif

class IMAGEPROCESSOR_API EyeFeatures

#else
class EyeFeatures
#endif
{
public:
	EyeFeatures();
	EyeFeatures(cv::RotatedRect* pupilBox, std::vector<cv::Point2f>* crReflections, std::vector<float>* crIntensities,  std::vector<int>* crSizes, float score = 0);
	EyeFeatures(cv::Point2f* pupilCenter, std::vector<cv::Point2f>* crReflections, std::vector<float>* crIntensities,  std::vector<int>* crSizes, float score = 0);
	EyeFeatures(EyeFeatures* pEyeFeatures, int HorFlipW = 0);

	cv::Point2f				  getPupilCenter();
	cv::RotatedRect			  getPupilBox();
	std::vector<cv::Point2f>  getCrReflections();
	std::vector<float>		  getCrIntensities();
	std::vector<int>		  getCrSizes();

	bool				 isValid();
	cv::Point2f          getCrReflection(unsigned int n);
	bool                 setCrReflection(unsigned int n, cv::Point2f CR);
	float                getCrIntensity(unsigned int n);
	int                  getCrSize(unsigned int n);
	void				 CrReflectionsAddOffset(cv::Point2f Off);
	void                 ScaleCrIntensities(float Scale, float MaxVal = 255);

	void				SetPupilCenter(cv::Point2f pupilCenter);
	void				SetPupilBox(cv::RotatedRect& pupil);
	float				getCrDist();

	void			 setValidity(bool isValid);
	void			 setScore(float score);
	void			 setPupLevel(float pupLevel);
	void			 setIrisLevel(float irisLevel);
	void			 setContrastLevel(float pupIrisContrast);
		
	float			 getScore() const;
	float			 getPupLevel() const;
	float			 getIrisLevel() const;
	float			 getContrastLevel() const;

private:
	cv::RotatedRect			 m_pupil;
	std::vector<cv::Point2f> m_crReflections;
	std::vector<float>		 m_crIntensities;
	std::vector<int>		 m_crSizes;
	bool					 m_bValid;

	float					 m_score;
	float					 m_pupLevel;
	float					 m_irisLevel;
	float					 m_pupIrisContrast;
};
