///////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//                           License Agreement
//
// This software is provided for evaluation purposes only. 
// Use in source and binary forms, with or without modification,
// is permitted with written consent of the copyright holder. 
//
// Redistribution of this software to third parties is strictly prohibited.
//
// This software is provided "as is" and without any express or implied warranties.
//
// By downloading, copying, installing or using the software you agree to this license.
// If you do not agree to this license, do not download, install,
// copy or use the software.
//
// To obtain a commercial license please contact the copyright holder.
//
// Copyright (C) 2013-2016, Eyediya Technologies Inc. (info@eyediya.ca)
// All rights reserved.
//
///////////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "opencv2\opencv.hpp"
#include "ImageProcessingResult.h"


#ifdef _WINDOWS
#ifdef  IMAGEPROCESSOR_EXPORTS
#define IMAGEPROCESSOR_API __declspec(dllexport) 
#else
#define IMAGEPROCESSOR_API __declspec(dllimport) 
#endif
#endif

class ImProcBase
{
public:
	virtual void				   processImage(cv::Mat_<uchar>& processImage, cv::Mat_<uchar>& displayImage, std::vector<cv::Rect>* pROIsVector = NULL) = 0;
	virtual ImageProcessingResult* getResultsPtr(bool UseFilter = false) = 0;
	virtual cv::Point2f            getHeadMovement() = 0;
	virtual void                   UpdateRightToLeftEyeVector(cv::Point2f RightToLeftEyeVector) = 0;

	virtual	bool				   SetDebugFileName(std::string DebugFileName) = 0;
	virtual	void				   EnableDebugOutput() = 0;
	virtual	void				   DisableDebugOutput() = 0;
};

#ifdef _WINDOWS
class IMAGEPROCESSOR_API ImageProcessor
#else
class ImageProcessor
#endif
{
public:
	ImageProcessor(float focal_length = 12, float LS_Dist = 30, bool PT_GREY_FLAG = false, int Flip = 0, uchar Scale = 1);
	~ImageProcessor();

	virtual void processImage(cv::Mat_<uchar>& processImage, cv::Mat_<uchar>& displayImage, std::vector<cv::Rect>* pROIsVector = NULL);
	virtual ImageProcessingResult* getResultsPtr(bool UseFilter = false);
	virtual cv::Point2f            getHeadMovement();
	virtual void                   UpdateRightToLeftEyeVector(cv::Point2f RightToLeftEyeVector);

	virtual	bool				   SetDebugFileName(std::string DebugFileName);
	virtual	void				   EnableDebugOutput();
	virtual	void				   DisableDebugOutput();

private:
	ImProcBase* pImageProcessor;
};

#ifdef _WINDOWS
void IMAGEPROCESSOR_API	FastResize(cv::Mat& inMat, cv::Mat& outMat);
#else
void FastResize(cv::Mat& inMat, cv::Mat& outMat);
#endif
