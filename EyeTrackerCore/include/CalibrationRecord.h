///////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//                           License Agreement
//
// This software is provided for evaluation purposes only. 
// Use in source and binary forms, with or without modification,
// is permitted with written consent of the copyright holder. 
//
// Redistribution of this software to third parties is strictly prohibited.
//
// This software is provided "as is" and without any express or implied warranties.
//
// By downloading, copying, installing or using the software you agree to this license.
// If you do not agree to this license, do not download, install,
// copy or use the software.
//
// To obtain a commercial license please contact the copyright holder.
//
// Copyright (C) 2013-2016, Eyediya Technologies Inc. (info@eyediya.ca)
// All rights reserved.
//
///////////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "EyeFeatures.h"
#include "HeadPose.h"

#ifdef _WINDOWS

#ifdef  CALIBRATOR_EXPORTS
#define CALIBRATOR_API __declspec(dllexport) 
#else
#define CALIBRATOR_API __declspec(dllimport) 
#endif

class CALIBRATOR_API CalibrationRecord

#else

class CalibrationRecord

#endif

{
public:
	CalibrationRecord(EyeFeatures* features, HeadPose* headpose, Point3D* calibPoint);
	CalibrationRecord(CalibrationRecord* record);

	EyeFeatures   getFeatures();
	HeadPose      getHeadPose();
	Point3D       getCalibPoint();

	EyeFeatures*  getFeaturesPtr() {return &m_features;};
	HeadPose*     getHeadPosePtr() {return &m_headPose;};

	~CalibrationRecord();

private:
	EyeFeatures m_features;
	HeadPose    m_headPose;
	Point3D     m_calibPoint;
};

