///////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//                           License Agreement
//
// This software is provided for evaluation purposes only. 
// Use in source and binary forms, with or without modification,
// is permitted with written consent of the copyright holder. 
//
// Redistribution of this software to third parties is strictly prohibited.
//
// This software is provided "as is" and without any express or implied warranties.
//
// By downloading, copying, installing or using the software you agree to this license.
// If you do not agree to this license, do not download, install,
// copy or use the software.
//
// To obtain a commercial license please contact the copyright holder.
//
// Copyright (C) 2013-2016, Eyediya Technologies Inc. (info@eyediya.ca)
// All rights reserved.
//
///////////////////////////////////////////////////////////////////////////////////////

#pragma once
#include <string>
#include "opencv2\opencv.hpp"
#include <Windows.h>

#ifdef GRAPHICWINDOW_EXPORTS
#define GRAPHICWINDOW_API __declspec(dllexport) 
#else
#define GRAPHICWINDOW_API __declspec(dllimport) 
#endif

class GRAPHICWINDOW_API GraphicWindow
{
public:
	GraphicWindow(const char* title, int cvWindowType = CV_WINDOW_NORMAL);
	~GraphicWindow();
	void displayImage(cv::Mat* image);
	void resizeGraphicWindow(unsigned int width, unsigned int height);
	void SetPosition(int xPos, int yPos);

	void HideWindow();
	void ShowWindow();
	std::string* GetWindowName() {return &m_title;};

	unsigned int  GetWidth()  {return m_width;};
	unsigned int  GetHeight() {return m_height;};

	void SetExitFlag() {m_ExitFlag = true; };
	bool GetExitFlag() {return m_ExitFlag;};

	void SetLastKey(int key) {m_LastKey = key; };
	int  GetLastKey()        {return m_LastKey;};

	void SetFocus() {::SetFocus(m_WindowHandle);};

	void SetTopMost();
	
	//void updateWindow() {cv::updateWindow(m_title.c_str());};
	WNDPROC      m_OrigOpenCVWinProc;

protected:
	std::string  m_title;
	unsigned int m_width;
	unsigned int m_height;
	int          m_xPos;
	int          m_yPos;
	int          m_cvWindowType;
	bool         m_isHidden;
	bool         m_isCustomSize;
	bool         m_isCustomPos;
	bool         m_ExitFlag;
	HWND		 m_WindowHandle;
	int			 m_LastKey;
};
