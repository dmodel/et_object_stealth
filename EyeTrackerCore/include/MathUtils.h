///////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//                           License Agreement
//
// This software is provided for evaluation purposes only. 
// Use in source and binary forms, with or without modification,
// is permitted with written consent of the copyright holder. 
//
// Redistribution of this software to third parties is strictly prohibited.
//
// This software is provided "as is" and without any express or implied warranties.
//
// By downloading, copying, installing or using the software you agree to this license.
// If you do not agree to this license, do not download, install,
// copy or use the software.
//
// To obtain a commercial license please contact the copyright holder.
//
// Copyright (C) 2013-2016, Eyediya Technologies Inc. (info@eyediya.ca)
// All rights reserved.
//
///////////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "opencv2\opencv.hpp"
#include <math.h>

namespace utils
{
	template<typename _Tp> int round(_Tp val)
	{
		return (int)(val + (val >= 0 ? 0.5 : -0.5));
	}

	template<typename _Tp> inline cv::Point round(cv::Point_<_Tp> pt)
	{
		return cv::Point(round(pt.x), round(pt.y));
	}

	template<typename _Tp> inline cv::Point ceil(cv::Point_<_Tp> pt)
	{
		return cv::Point(std::ceil(pt.x), std::ceil(pt.y));
	}

	template<typename _Tp> inline cv::Point floor(cv::Point_<_Tp> pt)
	{
		return cv::Point(std::floor(pt.x), std::floor(pt.y));
	}

	template<typename _Tp> inline _Tp rad2deg(_Tp angle_rad) 
	{
		return (_Tp)(angle_rad/CV_PI*180.0);
	}

	template<typename _Tp> inline _Tp deg2rad(_Tp angle_deg) 
	{
		return (_Tp)(angle_deg/180.0*CV_PI);
	}

}

