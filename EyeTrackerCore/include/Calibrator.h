///////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//                           License Agreement
//
// This software is provided for evaluation purposes only. 
// Use in source and binary forms, with or without modification,
// is permitted with written consent of the copyright holder. 
//
// Redistribution of this software to third parties is strictly prohibited.
//
// This software is provided "as is" and without any express or implied warranties.
//
// By downloading, copying, installing or using the software you agree to this license.
// If you do not agree to this license, do not download, install,
// copy or use the software.
//
// To obtain a commercial license please contact the copyright holder.
//
// Copyright (C) 2013-2016, Eyediya Technologies Inc. (info@eyediya.ca)
// All rights reserved.
//
///////////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "UserParameters.h"
#include "EyeParameters.h"
#include "GazeEstimator.h"
#include "opencv2\opencv.hpp"
#include "SystemParams.h"
#include "CalibrationDataPoint.h"
#include <vector>

#ifdef _WINDOWS

#ifdef  CALIBRATOR_EXPORTS
#define CALIBRATOR_API __declspec(dllexport) 
#else
#define CALIBRATOR_API __declspec(dllimport) 
#endif

#endif


class Calibrator_Base_Class
{
public:

	virtual UserParameters  getUserParameters() = 0;
	virtual UserParameters* getUserParametersPtr() = 0;
	virtual void addDataPoint(ImageProcessingResult* data) = 0;
	virtual void beginCalibration(bool waitForInput) = 0;
	virtual void endCalibration(bool shouldCalibrate, const char* UserName = NULL) = 0;

	virtual void SaveUserParamsToFile(std::string FolderName) = 0;
	virtual bool LoadUserParamsFromFile(std::string FolderName, std::string UserName) = 0;

	virtual bool allDataCollected() = 0;
	virtual bool isCalibrating()    = 0;
	virtual bool isCalibrated()     = 0;

	virtual void quitCalibration()  = 0;

	virtual bool isWaitingToContinue() = 0;
	virtual bool TogglePause() = 0;
	virtual void proceed() = 0;

	virtual bool GetLastCalibrationStatus() = 0;

	virtual std::vector<CalibrationDataPoint*>* GetLeftDataPtr() = 0;
	virtual std::vector<CalibrationDataPoint*>* GetRightDataPtr() = 0;

	virtual cv::Mat* GetCalibDisplayImagePtr() = 0;

	virtual void SaveEyeFeatures2File(std::vector<CalibrationDataPoint*> *pCalibData, std::string FileName) = 0;


protected:
	std::vector<CalibrationDataPoint*> m_leftEyeData;
	std::vector<CalibrationDataPoint*> m_rightEyeData;

};

#ifdef _WINDOWS

class CALIBRATOR_API Calibrator

#else

class Calibrator

#endif

{
public:
	Calibrator(GazeEstimator* estimator, unsigned int numPoints, int framesPerPoint, SystemParams* pSysParams); 
	~Calibrator();

	void beginCalibration(bool waitForInput);
	void addDataPoint(ImageProcessingResult* data);
	void endCalibration(bool shouldCalibrate, const char* UserName = NULL);
	void quitCalibration();

	bool allDataCollected();
	bool isCalibrating();
	bool isCalibrated();
	bool isWaitingToContinue();
	bool TogglePause();
	void proceed();
	bool GetLastCalibrationStatus();

	UserParameters  getUserParameters();
	UserParameters* getUserParametersPtr();

	void SaveUserParamsToFile(std::string FolderName);
	bool LoadUserParamsFromFile(std::string FolderName, std::string UserName);

	void SaveEyeFeatures2File(std::vector<CalibrationDataPoint*> *pCalibData, std::string FileName);

	std::vector<CalibrationDataPoint*>* GetLeftDataPtr();
	std::vector<CalibrationDataPoint*>* GetRightDataPtr();

	cv::Mat* GetCalibDisplayImagePtr();

private:

	Calibrator_Base_Class* m_pCalibrator;

};

