///////////////////////////////////////////////////////////////////////////////////////
//
//  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
//
//                           License Agreement
//
// This software is provided for evaluation purposes only. 
// Use in source and binary forms, with or without modification,
// is permitted with written consent of the copyright holder. 
//
// Redistribution of this software to third parties is strictly prohibited.
//
// This software is provided "as is" and without any express or implied warranties.
//
// By downloading, copying, installing or using the software you agree to this license.
// If you do not agree to this license, do not download, install,
// copy or use the software.
//
// To obtain a commercial license please contact the copyright holder.
//
// Copyright (C) 2013-2016, Eyediya Technologies Inc. (info@eyediya.ca)
// All rights reserved.
//
///////////////////////////////////////////////////////////////////////////////////////

#pragma once
#include "displayPlane.h"
#include "opencv2\opencv.hpp"

#include "MathDataTypes.h"
#include "MathUtils.h"

#ifdef _WINDOWS

#ifdef  SYSTEMPARAMS_EXPORTS
#define SYSTEMPARAMS_API __declspec(dllexport) 
#else
#define SYSTEMPARAMS_API __declspec(dllimport) 
#endif

class SYSTEMPARAMS_API Camera

#else

#include <jni.h>

class Camera

#endif

{
public:
	Camera(const Point3D& o, const Point3D& rotAngles_deg, 
		   const cv::Size& sensorSize_px, const Point2D& principalPoint_px, const Point2D& pixelPitch_mm_per_px, 
		   Real  nomFocalLength_mm, Real nomDistEyeCam_mm, const Point2D& effFocalLength_px,
		   const Vec8D& distCoeffs);

	Vec3D	  getNodalPtWCS()       {return m_o;};
	Real	  getNomDistEyeCam_mm() {return m_nomDistEyeCam_mm;};
	cv::Size  getSensorSize()       {return m_sensorSize_px;};

	void backProjImgPtWCS(const cv::Point2f& imgPt, Vec3D& backProjDir);

private:
	Vec3D   m_o;  // Nodal point of the camera w.r.t. WCS
	Point3D m_rotAngles_deg;  // Rotation angles of the camera in degrees w.r.t. WCS as defined in Elias' PhD thesis
	Matx3x3 m_Rcam;  // Rotation matrix of the CCS w.r.t. WCS
	// IMPORTANT: In Bouguet's CCS, x_CCS points to the left (looking into the camera), y_CCS points down, and z_CCS points forward (relative to the camera)
	// Also, the nodal point of the camera is BEHIND the image plane
	cv::Size m_sensorSize_px;  // Sensor size in pixels
	Point2D  m_principalPoint_px;  // Principal point in pixels
	Point2D  m_pixelPitch_mm_per_px;  // Pixel pitch in mm per pixel
	Real    m_nomFocalLength_mm;  // Nominal focal length in mm
	Real    m_nomDistEyeCam_mm;  // Nominal distance between the eye and the camera in mm - Used to calculate the effective focal length (lambda), and as an initial guess for the calculation of c
	Real    m_effFocalLength_mm;  // Effective focal length (lambda in Elias' PhD thesis) in mm - accounts for lens focusing
	Point2D m_effFocalLength_px;  // Effective focal length in pixels
	Matx3x3 m_cameraMatrix;  // Camera matrix
	Vec8D   m_distCoeffs;  // Lens distortion coefficients
	
	void    calcRcam();
};